<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template name="registratura">
        <div class="block">
            <div class="title">
                <xsl:value-of select="//REALM/TITLE"/>
            </div>
            <div class="content">
                <xsl:choose>
                    <xsl:when test="//REALM/@name='ru/reg'">
                        <form name="cal" action="" method="get">
                            <a href="/public/files/instrukciya-zapis-na-priem.doc">Как записаться к врачу? (скачать Инструкцию)</a><br/><br/>
                            <font color="#CC0000"><b>ВНИМАНИЕ!!! <br/>ЗАПИСАТЬСЯ НА ПРИЕМ МОГУТ ТОЛЬКО ПАЦИЕНТЫ ПРИКРЕПЛЕННЫЕ К УЗУНКОЛЬСКОЙ ЦРБ</b></font><br/><br/>
                            <table border="0px" cellpadding="0px" cellspacing="0px" width="100%">
                                <tr>
                                    <td width="50%" valign="top" style="font-family: Verdana; font-size: 14px; padding-top: 10px">
                                        <div style="float: left; padding: 4px 6px 0px 6px;" class="selectday">Выберите день работы:</div>
                                        <form name="cal" action="" method="get">
                                            <xsl:choose>
                                                <xsl:when test="//PARAM[@name='day']">
                                                    <input type="text" name="day" value="{//PARAM[@name='day']}" class="datepicker" onchange="cal.submit()" style="text-align: center;width: 160px; font-size: 22px; color: #C01618; cursor: pointer;"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <input type="text" name="day" value="{//DATE[@is_current='yes']/@fulldate}" class="datepicker" onchange="cal.submit()" style="text-align: center; width: 160px; font-size: 22px; color: #C01618; cursor: pointer;"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </form>
                                    </td>
                                    <td align="right" rowspan="2">
                                        <div style="display: inline-block; background-color: #F0F0F0; padding: 10px 0px 20px 12px">
                                            <div align="center" style="font-size: 42px; color: #C01618; padding-right: 12px"><xsl:value-of select="//CALENDAR/MONTH/WEEK/DATE[@is_current='yes']"/></div>
                                            <div align="center" style="font-size: 18px; color: #C01618; padding-right: 12px"><xsl:value-of select="//CALENDAR/MONTH/@name"/></div>
                                            <xsl:variable name="weekday" select="//CALENDAR/MONTH/WEEK/DATE[@is_current='yes']/@weekday"/>
                                            <div align="center" style="font-size: 16px; color: #000; padding-right: 12px"><xsl:value-of select="//CALENDAR/WEEKDAY[@weekday=$weekday]"/></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="spec" onchange="cal.submit()">
                                            <option value="0">Выберите специалиста</option>
                                            <xsl:for-each select="//LIST[@id='45']/LIST">
                                                <option value="{@id}">
                                                    <xsl:if test="//PARAM[@name='spec'] = @id">
                                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                                    </xsl:if>
                                                    <xsl:value-of select="@value"/>
                                                </option>
                                            </xsl:for-each>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <xsl:choose>
                            <xsl:when test="//CLOSED">
                                <B><font color="#CC0000">Запись на <xsl:value-of select="//DATE[@is_current='yes']/@fulldate"/> закрыта!</font></B>
                            </xsl:when>
                            <xsl:otherwise>
                                <table width="100%">
                                    <tr>
                                        <td class="doc_header">Доктор</td>
                                        <td class="doc_header">Специальность</td>
                                        <td class="doc_header">Время работы</td>
                                        <td class="doc_header"/>
                                    </tr>
                                    <xsl:choose>
                                        <xsl:when test="//PARAM[@name='spec'] and //PARAM[@name='spec'] != '0'">
                                            <xsl:variable name="sc" select="//PARAM[@name='spec']"/>
                                            <xsl:apply-templates select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/SCHEDULE[@spec=$sc]"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/SCHEDULE[@day=//PARAM[@name='day']]"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <!--xsl:variable name="sc" select="//PARAM[@name='spec']"/>
                                    <xsl:for-each select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR[SCHEDULE/@spec=$sc]">
                                        <xsl:variable name="login" select="@login"/>
                                        <tr>
                                            <td class="doc_name">
                                                <xsl:value-of select="@name"/>
                                            </td>
                                            <td valign="top" class="doc_name">
                                                <xsl:variable name="weekday" select="@weekday"/>
                                                <div class="worktime">
                                                    <xsl:choose>
                                                        <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE/MESSAGE">
                                                            <xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE/MESSAGE"/>
                                                        </xsl:when>
                                                        <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE/WORKTIME">
                                                            <xsl:variable name="spec" select="//DOCTOR[@login=$login]/SCHEDULE/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/>&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </div>
                                            </td>
                                            <td class="doc_name">
                                                <xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE/WORKTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE/WORKTIME/@to"/>
                                            </td>
                                            <td class="doc_name">
                                                <a href="/ru/add?doctor={$login}&amp;day={//DOCTOR[@login=$login]/SCHEDULE/@day}&amp;schedule={//DOCTOR[@login=$login]/SCHEDULE/@schedule}">Записаться</a>
                                            </td>
                                        </tr>
                                    </xsl:for-each-->
                                </table>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="//REALM/@name='ru/add'">
                        <xsl:if test="//ERROR">
                            <div class="error"><xsl:value-of select="//ERROR"/></div>
                        </xsl:if>
                        <form name="talonset" method="post">
                            <div class="talon-block">
                                <B>Доктор:</B>&#xA0;<xsl:value-of select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/@name"/>
                                <input type="hidden" name="doctor" value="{/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/@login}"/>
                                <input type="hidden" name="schedule" value="{//PARAM[@name='schedule']}"/>
                            </div>
                            <div class="talon-block">
                                <xsl:variable name="spec" select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/SCHEDULE/@spec"/>
                                <B>Специальность:</B>&#xA0;<xsl:value-of select="//LIST[@id=$spec]/@value"/>
                            </div>
                            <div class="talon-block">
                                <B>Дата приема:</B>&#xA0;<xsl:value-of select="//PARAM[@name='day']"/>
                                <input type="hidden" name="day" value="{//PARAM[@name='day']}" id="datepicker"/>
                            </div>
                            <div class="talon-block">
                                <B>Выберите талон:</B>&#xA0;
                                <xsl:choose>
                                    <xsl:when test="/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/@in_time='1'">
                                        <xsl:for-each select="/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/TIME">
                                            <div>
                                                <xsl:choose>
                                                    <xsl:when test="@close='yes'">
                                                        <B><xsl:value-of select="."/> - №<xsl:value-of select="@tnum"/>&#xA0;-&#xA0;Занято</B>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="."/> - №<xsl:value-of select="@tnum"/>
                                                        <input type="radio" name="time" value="{@id}" class="timeselect"/>
                                                        <input type="radio" id="{@id}" name="tnum" value="{@tnum}" style="display: none"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </div>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each select="/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/TIME">
                                            <div>
                                                <xsl:choose>
                                                    <xsl:when test="@close='yes'">
                                                        <B>№<xsl:value-of select="@tnum"/>&#xA0;-&#xA0;Занято</B>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        №<xsl:value-of select="@tnum"/>
                                                        <input type="radio" name="time" value="{@id}" class="timeselect"/>
                                                        <input type="radio" id="{@id}" name="tnum" value="{@tnum}" style="display: none"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </div>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                            <xsl:if test="count(/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/TIME[not(@close)]) &gt; 0">
                                <input type="submit" value="Зарезервировать"/>
                            </xsl:if>
                        </form>
                    </xsl:when>
                    <xsl:when test="//REALM/@name='ru/edit'">
                        <form name="talonadd" method="post" action="">
                            <input type="hidden" name="id" value="{//PARAM[@name='id']}"/>
                            <table cellspacing="5px">
                                <tr>
                                    <td>ФИО</td>
                                    <td><input type="text" name="fio" style="width: 300px" value="{//FIELD[@id='fio']}"/></td>
                                </tr>
                                <tr>
                                    <td>Дата рождения</td>
                                    <td><input type="text" name="birth" style="width: 300px" value="{//FIELD[@id='birth']}"/></td>
                                </tr>
                                <tr>
                                    <td>Адрес</td>
                                    <td><input type="text" name="addr" style="width: 300px" value="{//FIELD[@id='addr']}"/></td>
                                </tr>
                                <tr>
                                    <td>Дата приема</td>
                                    <td>
                                        <div><xsl:value-of select="//TALON/@dday"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Время приема</td>
                                    <td>
                                        <div>
                                            <xsl:choose>
                                                <xsl:when test="//SCHEDULE/@in_time='0'">
                                                    Талон № <xsl:value-of select="//TALON/@tnum"/> без времени
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="//TALON/@daytime"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">Врач</td>
                                    <td style="padding-bottom: 10px">
                                        <xsl:value-of select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/@name"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">Специальность</td>
                                    <td style="padding-bottom: 10px">
                                        <xsl:variable name="spec" select="/DOCUMENT/INCLUDE[@name='Registratura']/SCHEDULE/@spec"/>
                                        <xsl:value-of select="//LIST[@id=$spec]/@value"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" value="Записать"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </xsl:when>
                    <xsl:when test="//REALM/@name='ru/completed'">
                        <xsl:call-template name="talon-form"/>
                    </xsl:when>
                    <xsl:when test="//REALM/@name = 'ru/schedule'">
                        <div align="left" valign="top" style="padding: 2px 6px 0px 0px; font-family: Verdana; font-size: 14px;">
                            День работы:
                        </div>
                        <div>
                            <form name="cal" action="" method="get" id="schedule-form">
                                <xsl:choose>
                                    <xsl:when test="//PARAM[@name='day']">
                                        <input type="text" name="day" value="{//PARAM[@name='day']}" class="datepicker" id="schedulecal" style="text-align: center;width: 160px; font-size: 22px; color: #C01618; cursor: pointer;"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <input type="text" name="day" value="{//DATE[@is_current='yes']/@fulldate}" class="datepicker" id="schedulecal" style="text-align: center; width: 160px; font-size: 22px; color: #C01618; cursor: pointer;"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </form>
                        </div>

                        <table class="schedule-table table-condensed table-striped table" border="0px" cellpadding="5px" cellspacing="0px" width="100%">
                            <tr>
                                <td class="header" valign="top"/>
                                <xsl:for-each select="//CALENDAR/MONTH/WEEK[DATE[@is_current='yes']]/DATE[@weekday]">
                                    <td class="header" align="center" width="12%">
                                        <xsl:variable name="weekday" select="@weekday"/>
                                        <div style="font-size: 22px;"><xsl:value-of select="."/></div>
                                        <div>
                                            <xsl:variable name="month" select="@month"/>
                                            <xsl:value-of select="//MONTHNAME[@num=$month]"/>
                                        </div>
                                        <div style="font-size: 10px;"><xsl:value-of select="//CALENDAR/WEEKDAY[@weekday=$weekday]"/></div>
                                    </td>
                                </xsl:for-each>
                            </tr>
                            <xsl:for-each select="//INCLUDE[@name='Registratura']/DOCTOR[SCHEDULE]">
                                <xsl:variable name="login" select="@login"/>
                                <tr>
                                    <td valign="top">
                                        <xsl:value-of select="@name"/>
                                        <xsl:if test="@params!=''">
                                            <div style="font-size: 11px; padding: 5px; color: grey">
                                                <xsl:value-of select="@params"/>
                                            </div>
                                        </xsl:if>
                                    </td>
                                    <xsl:for-each select="//CALENDAR//WEEK[DATE[@is_current='yes']]/DATE[@weekday]">
                                        <td valign="top" align="center">
                                            <xsl:variable name="cday" select="@fulldate"/>
                                            <div class="worktime">
                                                <xsl:choose>
                                                    <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/MESSAGE">
                                                        <xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/MESSAGE"/>
                                                    </xsl:when>
                                                    <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/WORKTIME">
                                                        <!--div>Время работы:&#xA0;</div>
                                                        <div><xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/WORKTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/WORKTIME/@to"/></div>
                                                        <div>Время приема:&#xA0;</div>
                                                        <div><xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@to"/></div>
                                                        <div>Специальность:&#xA0;</div>
                                                        <div><xsl:variable name="spec" select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/></div>
                                                        <xsl:if test="@past = 'no'">
                                                            <div><a href="/ru/add?doctor={$login}&amp;day={$cday}&amp;schedule={//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@id}">записаться</a></div>
                                                        </xsl:if-->
                                                        <div><xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@to"/></div>
                                                        <div><xsl:variable name="spec" select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/></div>
                                                        <xsl:if test="@past = 'no'">
                                                            <div><a href="/ru/add?doctor={$login}&amp;day={$cday}&amp;schedule={//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@id}">записаться</a></div>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        нет приема
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </div>
                                        </td>
                                    </xsl:for-each>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </xsl:when>
                </xsl:choose>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="regkz">
        <div class="block">
            <div class="block_header">
                <xsl:value-of select="//REALM/TITLE"/>
            </div>
            <div class="content">
                <xsl:choose>
                    <xsl:when test="//REALM/@name='kz/reg'">
                        <form name="cal" action="" method="get">
                            <a href="/files/elfinder/zapis/Instrukciya_Internet-registratura_Poliklinika1_kaz.doc">Дәрігерге қалай  жазылуға болады екен? (нұсқаулықты шығарып алу)</a><br/><br/>
                            <font color="#CC0000"><b>Назар аударыңыздар! <br/>Қабылдауға тек қана №1 емханаға тіркелген емделушілер жазыла алады.</b></font><br/><br/>
                            <table border="0px" cellpadding="0px" cellspacing="0px" width="100%">
                                <tr>
                                    <td width="50%" valign="top" style="font-family: Verdana; font-size: 14px; padding-top: 10px">
                                        <div style="float: left; padding: 4px 6px 0px 6px;">Жұмыс күнін таңдаңыз:</div>
                                        <form name="cal" action="" method="get">
                                            <xsl:choose>
                                                <xsl:when test="//PARAM[@name='day']">
                                                    <input type="text" name="day" value="{//PARAM[@name='day']}" id="datepicker" onchange="cal.submit()" style="text-align: center; height: 24px; width: 120px; border: 0px; font-size: 22px; color: #C01618; cursor: pointer; border: 1px solid #C01618;"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <input type="text" name="day" value="{//DATE[@is_current='yes']/@fulldate}" id="datepicker" onchange="cal.submit()" style="text-align: center; height: 24px; width: 120px; border: 0px; font-size: 22px; color: #C01618; cursor: pointer; border: 1px solid #C01618;"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </form>
                                    </td>
                                    <td align="right" rowspan="2">
                                        <div style="display: inline-block; background-color: #F0F0F0; padding: 10px 0px 20px 12px">
                                            <div align="center" style="font-size: 42px; color: #C01618; padding-right: 12px"><xsl:value-of select="//CALENDAR/MONTH/WEEK/DATE[@is_current='yes']"/></div>
                                            <div align="center" style="font-size: 18px; color: #C01618; padding-right: 12px"><xsl:value-of select="//CALENDAR/MONTH/@name"/></div>
                                            <xsl:variable name="weekday" select="//CALENDAR/MONTH/WEEK/DATE[@is_current='yes']/@weekday"/>
                                            <div align="center" style="font-size: 16px; color: #000; padding-right: 12px"><xsl:value-of select="//CALENDAR/WEEKDAY[@weekday=$weekday]"/></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Маманды таңданыз:
                                        <select name="spec" onchange="cal.submit()">
                                            <xsl:for-each select="//LIST[@id='2']/LIST">
                                                <option value="{@id}">
                                                    <xsl:if test="//PARAM[@name='spec'] = @id">
                                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                                    </xsl:if>
                                                    <xsl:value-of select="@value"/>
                                                </option>
                                            </xsl:for-each>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <table  width="100%">
                            <tr>
                                <td class="doc_header">Қабылдайтын дәрігер</td>
                                <td class="doc_header">Маманды таңданыз</td>
                                <td class="doc_header">Жұмыс уақыты</td>
                                <td class="doc_header"/>
                            </tr>
                            <xsl:variable name="sc" select="//PARAM[@name='spec']"/>
                            <xsl:for-each select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR[SCHEDULE/@spec=$sc]">
                                <xsl:variable name="login" select="@login"/>
                                <tr>
                                    <td class="doc_name">
                                        <xsl:value-of select="@name"/>
                                    </td>
                                    <td valign="top" class="doc_name">
                                        <xsl:variable name="weekday" select="@weekday"/>
                                        <div class="worktime">
                                            <xsl:choose>
                                                <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE/MESSAGE">
                                                    <xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE/MESSAGE"/>
                                                </xsl:when>
                                                <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE/WORKTIME">
                                                    <xsl:variable name="spec" select="//DOCTOR[@login=$login]/SCHEDULE/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/>&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;
                                                </xsl:when>
                                            </xsl:choose>
                                        </div>
                                    </td>
                                    <td class="doc_name">
                                        <xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE/WORKTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE/WORKTIME/@to"/>
                                    </td>
                                    <td class="doc_name">
                                        <a href="/kz/add?doctor={$login}&amp;day={//DOCTOR[@login=$login]/SCHEDULE/@day}&amp;schedule={//DOCTOR[@login=$login]/SCHEDULE/@schedule}">Жазылу</a>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </xsl:when>
                    <xsl:when test="//REALM/@name='kz/add'">
                        <xsl:if test="//ERROR">
                            <div class="error"><xsl:value-of select="//ERROR"/></div>
                        </xsl:if>
                        <form name="talonset" method="post">
                            <div class="talon-block">
                                <B>Қабылдайтын дәрігер:</B>&#xA0;<xsl:value-of select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/@name"/>
                                <input type="hidden" name="doctor" value="{/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/@login}"/>
                                <input type="hidden" name="schedule" value="{//PARAM[@name='schedule']}"/>
                            </div>
                            <div class="talon-block">
                                <xsl:variable name="spec" select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/SCHEDULE/@spec"/>
                                <B>Маманды таңданыз:</B>&#xA0;<xsl:value-of select="//LIST[@id=$spec]/@value"/>
                            </div>
                            <div class="talon-block">
                                <B>Қабылдау күні:</B>&#xA0;<xsl:value-of select="//PARAM[@name='day']"/>
                                <input type="hidden" name="day" value="{//PARAM[@name='day']}" id="datepicker"/>
                            </div>
                            <div class="talon-block">
                                <B>Талон:</B>&#xA0;
                                <xsl:choose>
                                    <xsl:when test="/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/@in_time='0'">
                                        <xsl:for-each select="/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/TIME">
                                            <div>
                                                <input type="radio" name="time" value="{@id}" class="timeselect"/>
                                                №<xsl:value-of select="@tnum"/>
                                                <input type="radio" id="{@id}" name="tnum" value="{@tnum}" style="display: none"/>
                                            </div>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:for-each select="/DOCUMENT/INCLUDE[@name='Registratura']/TIMESET/TIME">
                                            <div>
                                                <input type="radio" name="time" value="{@id}" class="timeselect"/>
                                                <xsl:value-of select="."/> - №<xsl:value-of select="@tnum"/>
                                                <input type="radio" id="{@id}" name="tnum" value="{@tnum}" style="display: none"/>
                                            </div>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                            <input type="submit" value="Кейінге қалдыру"/>
                        </form>
                    </xsl:when>
                    <xsl:when test="//REALM/@name='kz/edit'">
                        <form name="talonadd" method="post" action="">
                            <input type="hidden" name="id" value="{//PARAM[@name='id']}"/>
                            <table cellspacing="5px">
                                <tr>
                                    <td>Тегі Аты Әкесінің аты</td>
                                    <td><input type="text" name="fio" style="width: 300px" value="{//FIELD[@id='fio']}"/></td>
                                </tr>
                                <tr>
                                    <td>Туған жылы </td>
                                    <td><input type="text" name="birth" style="width: 300px" value="{//FIELD[@id='birth']}"/></td>
                                </tr>
                                <tr>
                                    <td>Адрес</td>
                                    <td><input type="text" name="addr" style="width: 300px" value="{//FIELD[@id='addr']}"/></td>
                                </tr>
                                <tr>
                                    <td>Қабылдау күні</td>
                                    <td>
                                        <div><xsl:value-of select="//TALON/@dday"/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Жұмыс уақыты</td>
                                    <td>
                                        <div>
                                            <xsl:choose>
                                                <xsl:when test="//SCHEDULE/@in_time='0'">
                                                    Талон № <xsl:value-of select="//TALON/@tnum"/> без времени
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="//TALON/@daytime"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">Қабылдайтын дәрігер</td>
                                    <td style="padding-bottom: 10px">
                                        <xsl:value-of select="/DOCUMENT/INCLUDE[@name='Registratura']/DOCTOR/@name"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">Маманды таңданыз</td>
                                    <td style="padding-bottom: 10px">
                                        <xsl:variable name="spec" select="/DOCUMENT/INCLUDE[@name='Registratura']/SCHEDULE/@spec"/>
                                        <xsl:value-of select="//LIST[@id=$spec]/@value"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" value="Записать"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </xsl:when>
                    <xsl:when test="//REALM/@name='kz/completed'">
                        <xsl:call-template name="talon-form"/>
                    </xsl:when>
                    <xsl:when test="//REALM/@name = 'kz/schedule'">
                        <div align="left" valign="top" style="padding: 2px 6px 0px 0px; font-family: Verdana; font-size: 14px;">
                            Қабылдау күні:
                        </div>
                        <div>
                            <form name="cal" action="" method="get">
                                <xsl:choose>
                                    <xsl:when test="//PARAM[@name='day']">
                                        <input type="text" name="day" value="{//PARAM[@name='day']}" id="datepicker" onchange="cal.submit()" style="height: 24px; width: 120px; border: 0px; font-size: 22px; color: #C01618"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <input type="text" name="day" value="{//DATE[@is_current='yes']/@fulldate}" id="datepicker" onchange="cal.submit()" style="height: 24px; width: 120px; border: 0px; font-size: 22px; color: #C01618"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </form>
                        </div>

                        <table class="schedule-table" border="0px" cellpadding="5px" cellspacing="0px" width="100%">
                            <tr>
                                <td class="header" valign="top"></td>
                                <xsl:for-each select="//CALENDAR/MONTH/WEEK[DATE[@is_current='yes']]/DATE[@weekday]">
                                    <td class="header" align="center" width="12%">
                                        <xsl:variable name="weekday" select="@weekday"/>
                                        <div style="font-size: 22px;"><xsl:value-of select="."/></div>
                                        <div>
                                            <xsl:variable name="month" select="@month"/>
                                            <xsl:value-of select="//MONTHNAME[@num=$month]"/>
                                        </div>
                                        <div style="font-size: 10px;"><xsl:value-of select="//CALENDAR/WEEKDAY[@weekday=$weekday]"/></div>
                                    </td>
                                </xsl:for-each>
                            </tr>
                            <xsl:for-each select="//INCLUDE[@name='Registratura']/DOCTOR[SCHEDULE]">
                                <xsl:variable name="login" select="@login"/>
                                <tr>
                                    <td valign="top">
                                        <xsl:value-of select="@name"/>
                                    </td>
                                    <xsl:for-each select="//CALENDAR//WEEK[DATE[@is_current='yes']]/DATE">
                                        <td valign="top">
                                            <xsl:variable name="cday" select="@fulldate"/>
                                            <div class="worktime">
                                                <xsl:choose>
                                                    <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/MESSAGE">
                                                        <xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/MESSAGE"/>
                                                    </xsl:when>
                                                    <xsl:when test="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/WORKTIME">
                                                        <!--div>Время работы:&#xA0;</div>
                                                        <div><xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/WORKTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/WORKTIME/@to"/></div>
                                                        <div>Время приема:&#xA0;</div>
                                                        <div><xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@to"/></div>
                                                        <div>Специальность:&#xA0;</div>
                                                        <div><xsl:variable name="spec" select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/></div>
                                                        <xsl:if test="@past = 'no'">
                                                            <div><a href="/ru/add?doctor={$login}&amp;day={$cday}&amp;schedule={//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@id}">записаться</a></div>
                                                        </xsl:if-->
                                                        <div><xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@from"/>-<xsl:value-of select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/RECIPIENTTIME/@to"/></div>
                                                        <div><xsl:variable name="spec" select="//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/></div>
                                                        <xsl:if test="@past = 'no'">
                                                            <div><a href="/kz/add?doctor={$login}&amp;day={$cday}&amp;schedule={//DOCTOR[@login=$login]/SCHEDULE[@day=$cday]/@id}">кесте</a></div>
                                                        </xsl:if>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        кесте жоқ
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </div>
                                        </td>
                                    </xsl:for-each>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </xsl:when>
                </xsl:choose>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="talon-form">
        <div class="talon-printable">
            <table cellpadding="5px" cellspacing="0px" border="0px" width="600px">
                <tr>
                    <td class="cart-header" width="25%">
                        Қазақстан Республикасы
                        Денсаулық сақтау министрлігі
                        Министерство здравоохранения
                        Республики Казахстан
                    </td>
                    <td rowspan="2" width="50%" align="center">
                        Дәрігердің қабылдауына арналған<BR/>
                        ТАЛОН&#xA0;№&#xA0;<font size="12px"><xsl:value-of select="//TALON/FIELD[@name='tnum']"/></font><BR/>
                        на прием к врачу<BR/>
                    </td>
                    <td class="cart-header" width="25%">
                        Қазақстан Республикасы
                        Денсаулық сақтау министрінің м.а. 2010 жылғы
                        «23» қарашадағы № 907 бұйрығымен бекітілген
                        № 025-4/е нысанды медициналық құжаттама
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="cart-header">
                        УЗУНКОЛЬСКАЯ ЦЕНТРАЛЬНАЯ
                        РАЙОННАЯ БОЛЬНИЦА КГП
                    </td>
                    <td class="cart-header">
                        Медицинская документация
                        Форма 025-4/у
                        Утверждена приказом Министра здравоохранения
                        Республики Казахстан «23» ноября  2010 года № 907
                    </td>
                </tr>
            </table>
            <table cellpadding="3px" cellspacing="0px" width="600px" border="0px">
                <tr>
                    <td class="printable" width="65%">1. Тегі, аты, әкесінің аты  (Фамилия, имя, отчество)</td>
                    <td class="printable"><xsl:value-of select="//TALON/FIELD[@name='f']"/>&#xA0;<xsl:value-of select="//TALON/FIELD[@name='i']"/>&#xA0;<xsl:value-of select="//TALON/FIELD[@name='o']"/></td>
                </tr>
                <tr>
                    <td class="printable">2. Туған күні (Дата рождения)</td>
                    <td class="printable"><xsl:value-of select="//TALON/FIELD[@name='birth']"/></td>
                </tr>
                <tr>
                    <td class="printable">3. Мекенжайы  (Адрес местожительства)</td>
                    <td class="printable"><xsl:value-of select="//TALON/FIELD[@name='addr']"/></td>
                </tr>
                <tr>
                    <td class="printable">4. Амбулаторлық науқастың медициналық  картасының № <BR/>(№ медицинской карты амбулаторного больного)</td>
                    <td class="printable"><xsl:value-of select="//TALON/FIELD[@name='cartnum']"/></td>
                </tr>
                <tr>
                    <td class="printable">5. Кабинет (Кабинет) №</td>
                    <td class="printable"><xsl:value-of select="//SCHEDULE/@cab"/></td>
                </tr>
                <tr>
                    <td class="printable">6. Келуі (Явиться)*</td>
                    <td class="printable"><xsl:value-of select="//TALON/FIELD[@name='dday']"/>
                        <xsl:if test="//INCLUDE[@name='Registratura']/SCHEDULE/@in_time=1">
                            в&#xA0;<xsl:value-of select="//TALON/FIELD[@name='time']"/>
                        </xsl:if>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="printable">7. Дәрігерге (К врачу) </td>
                    <td class="printable">
                        <xsl:value-of select="//DOCTOR/@name"/>&#xA0;(<xsl:variable name="spec" select="//SCHEDULE/@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/>)
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="printable">8. Возраст</td>
                    <td style="padding-bottom: 10px" class="printable">
                        <xsl:choose>
                            <xsl:when test="//TALON/FIELD[@name='age'] = 1">
                                Бала (Ребенок) (0-14 жастағы) (0-14 лет включительно)
                            </xsl:when>
                            <xsl:when test="//TALON/FIELD[@name='age'] = 2">
                                Подросток(от 15 до 17)
                            </xsl:when>
                            <xsl:when test="//TALON/FIELD[@name='age'] = 3">
                                ересек (взрослый) (18 жастан жоғары) (от 18 и старше)
                            </xsl:when>
                        </xsl:choose>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="printable">9. Келу себебі (Повод обращения)</td>
                    <td class="printable">
                        <xsl:choose>
                            <xsl:when test="//TALON/FIELD[@name='povod'] = 1">
                                сырқаты (заболевание)
                            </xsl:when>
                            <xsl:when test="//TALON/FIELD[@name='povod'] = 2">
                                профилактикалық тексеру (профилактический осмотр)
                            </xsl:when>
                            <xsl:when test="//TALON/FIELD[@name='povod'] = 3">
                                екпе (прививка)
                            </xsl:when>
                            <xsl:when test="//TALON/FIELD[@name='povod'] = 4">
                                анықтамалық алуға (за справкой)
                            </xsl:when>
                            <xsl:when test="//TALON/FIELD[@name='povod'] = 5">
                                басқа себептер (другие причины)
                            </xsl:when>
                        </xsl:choose>
                    </td>
                </tr>
                <xsl:if test="//TALON/FIELD[@name='text'] != ''">
                    <tr>
                        <td valign="top" class="printable">
                            (керегінің астын сызыңыз, жетпегенін жазыңыз) (нужное подчеркнуть, недостающее вписать).
                        </td>
                        <td class="printable"><xsl:value-of select="//TALON/FIELD[@name='text']"/></td>
                    </tr>
                </xsl:if>
            </table>
            <div style="font-size: 18px; width: 600px" align="center">
                *Прием производится по порядку номеров талонов <br/>
                <input type="button" class="print" value="Распечатать" onclick="window.print();return false;"/>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="SCHEDULE">
        <xsl:variable name="login" select="@login"/>
        <tr>
            <td class="doc_name">
                <xsl:value-of select="//DOCTOR[@login=$login]/@name"/>
                <xsl:if test="//DOCTOR[@login=$login]/@params!=''">
                    <div style="font-size: 11px; padding: 5px; color: grey">
                        <xsl:value-of select="//DOCTOR[@login=$login]/@params"/>
                    </div>
                </xsl:if>
            </td>
            <td valign="top" class="doc_name">
                <xsl:variable name="weekday" select="@weekday"/>
                <div class="worktime">
                    <xsl:choose>
                        <xsl:when test="MESSAGE">
                            <xsl:value-of select="MESSAGE"/>
                        </xsl:when>
                        <xsl:when test="WORKTIME">
                            <xsl:variable name="spec" select="@spec"/><xsl:value-of select="//LIST[@id=$spec]/@value"/>&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;
                        </xsl:when>
                    </xsl:choose>
                </div>
            </td>
            <td class="doc_name">
                <xsl:value-of select="RECIPIENTTIME/@from"/>-<xsl:value-of select="RECIPIENTTIME/@to"/>
            </td>
            <td class="doc_name">
                <a href="/ru/add?doctor={$login}&amp;day={@day}&amp;schedule={@schedule}">Записаться</a>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>