<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template name="cart">
        <div class="block">
            <div class="title">
                <xsl:value-of select="//REALM/TITLE"/>
            </div>
            <div class="content">
                <xsl:choose>
                    <xsl:when test="//REALM/@name='cart/orders'">
                        <xsl:if test="//INCLUDE[@name='Cart']/MSG">
                            <div class="msg">
                                <xsl:value-of select="//INCLUDE[@name='Cart']/MSG"/>
                            </div>
                        </xsl:if>
                        <table cellpadding="0px" cellspacing="0px" border="0px" width="100%" style="margin-top: 10px" class="tblcart">
                            <tr>
                                <th class="table-header">№</th>
                                <th class="table-header">Дата</th>
                                <th class="table-header">Сумма заказа</th>
                                <th class="table-header">Статус</th>
                                <th class="table-header">Накладная</th>
                            </tr>
                            <xsl:for-each select="//INCLUDE[@name='Cart']/ORDERS/ORDER">
                                <tr class="table-body">
                                    <td style="padding: 5px" valign="top">
                                        <xsl:value-of select="@id"/>
                                    </td>
                                    <td style="padding: 5px" valign="top">
                                        <xsl:value-of select="@date"/>
                                    </td>
                                    <td style="padding: 5px" valign="top">
                                        <!--xsl:choose>
                                            <xsl:when test="@percent">
                                                <xsl:value-of select="@dtotal"/>&#xA0;тнг.
                                            </xsl:when>
                                            <xsl:otherwise-->
                                        <xsl:value-of select="@total"/>&#xA0;тнг.
                                        <!--/xsl:otherwise>
                                    </xsl:choose-->
                                    </td>
                                    <td style="padding: 5px" valign="top">
                                        <xsl:choose>
                                            <xsl:when test="@status = 0">В обработке</xsl:when>
                                            <xsl:when test="@status = 1">Ожидание оплаты</xsl:when>
                                            <xsl:when test="@status = 2">Заказ оплачен</xsl:when>
                                            <xsl:when test="@status = 3">Заказ отправлен</xsl:when>
                                            <xsl:when test="@status = 4">Заказ получен</xsl:when>
                                            <xsl:when test="@status = 5">Отменен</xsl:when>
                                            <xsl:when test="@status = 6">Заказ комплектуется</xsl:when>
                                        </xsl:choose>
                                    </td>
                                    <td style="padding: 5px" valign="top">
                                        <div class="invoice">
                                            <div class="show-invoice">
                                                Показать заказ
                                            </div>
                                            <div class="invoice-form" id="{@id}">
                                                <div style="float: left">
                                                    Заказ № <xsl:value-of select="@id"/>
                                                </div>
                                                <div class="invoice-close" onclick="$('#'+{@id}).toggle()">
                                                    <img src="/i/delete.png" width="10px"/>
                                                </div>
                                                <div style="clear: both; height: 5px"/>
                                                <xsl:value-of select="." disable-output-escaping="yes"/>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="block">
                            <div class="title" align="left"><div class="wrapper title-content">Корзина заказа</div></div>
                            <xsl:choose>
                                <xsl:when test="//PARAMSET[@name='cart']/PARAM">
                                    <xsl:choose>
                                        <xsl:when test="//INCLUDE[@name='Cart']/MSG/CART/@send">
                                            <div class="content">
                                                Вы успешно подтвердили заказ. Теперь вы можете следить за его состоянием в разделе "<a href="/cart/orders">История заказов</a>"
                                            </div>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <table cellpadding="5px" cellspacing="0px" border="0px" width="100%" style="margin: 10px 0px;" class="tblcart">
                                                <tr>
                                                    <th class="table-header" align="left">Наименование</th>
                                                    <th class="table-header" align="left">Размер</th>
                                                    <th class="table-header" align="left">Цена</th>
                                                    <th class="table-header" align="left">Количество</th>
                                                    <th class="table-header" align="left">Стоимость</th>
                                                    <th class="table-header" width="10px" align="left"><!--img src="/i/admin/delete.png" class="delete-all" width="22px"/--></th>
                                                </tr>
                                                <xsl:for-each select="//INCLUDE[@name='Cart']/CART/ORDER">
                                                    <tr>
                                                        <td class="table-body tblborder">
                                                            <xsl:variable name="realm" select="NODE/@realm"/>
                                                            <a href="/{//SECTION[@realm = $realm]/@name}?node={NODE/@node}">
                                                                <xsl:value-of select="NODE/@name"/>
                                                            </a>
                                                        </td>
                                                        <td class="table-body tblborder">
                                                            <xsl:value-of select="NODE/DATATYPE/FIELD[@name='title']/DATA"/>
                                                        </td>
                                                        <td class="table-body tblborder" align="center"><xsl:value-of select="@price1"/>&#xA0;тг.</td>
                                                        <td class="table-body tblborder">
                                                            <input type="text" name="count" value="{@count}" class="cart-count" item-id="{@id}" style="width: 30px" price-count="{NODE/DATATYPE/FIELD[@name='price']/DATA/@count}"/>
                                                            <div class="cart-alert">
                                                                <xsl:if test="@count &gt; NODE/DATATYPE/FIELD[@name='price']/DATA/@count">
                                                                    <xsl:attribute name="style">display: block</xsl:attribute>
                                                                </xsl:if>
                                                                <img src="/i/x.gif" width="16px" height="16px"/>
                                                            </div>
                                                            <!--div class="cart-alert-popup">В наличии <xsl:value-of select="NODE/DATATYPE/FIELD[@name='price']/DATA/@count"/> штук, остальное под заказ.</div-->
                                                        </td>
                                                        <td class="table-body tblborder cost"><xsl:value-of select="@total"/>&#xA0;тг.</td>
                                                        <td class="table-body tblborder"><img src="/i/delete.png" class="cartorder-delete" id="{@id}" width="10px" style="cursor:pointer;"/></td>
                                                    </tr>
                                                </xsl:for-each>
                                                <tr>
                                                    <td></td>
                                                    <td align="center"><div class="reload cabinet-btn">Пересчитать</div></td>
                                                    <td class="table-header" style="border: 0px; font-size: 14px" align="right">Итого:</td>
                                                    <td class="table-header itogocart" style="border: 0px; font-size: 14px" colspan="2" align="left"><xsl:value-of select="//TOTAL"/>&#xA0;тг.</td>
                                                </tr>
                                            </table>
                                            <table width="100%">
                                                <tr valign="top">
                                                    <td width="50%">
                                                        <!--div style="font-size: 12px; margin-bottom: 10px;">
                                                            При заказе в нашем интернет-магазине применяется <a href="/skidka"><b>Система скидок</b></a>. Для расчета скидки нажмите "Пересчитать".
                                                        </div-->
                                                    </td>
                                                    <td width="50%">
                                                        <xsl:choose>
                                                            <xsl:when test="//SESSION/@login = ''">
                                                                <div style="font-size: 12px; margin-bottom: 10px;">
                                                                    Для оформления заказа, Вам, необходимо войти в <b>Личный кабинет</b> (справа) или <a href="/reg"><b>Зарегистрируйтесь</b></a>.
                                                                </div>
                                                            </xsl:when>
                                                            <xsl:when test="//SESSION/@login = 'nobody'">
                                                                <div style="font-size: 12px; margin-bottom: 10px;">
                                                                    Для оформления заказа, Вам, необходимо войти в <b>Личный кабинет</b> (справа) или <a href="/reg"><b>Зарегистрируйтесь</b></a>.
                                                                </div>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <a href="/cart?submit=yes"><div class="cartorder-btn cabinet-btn">Заказать</div></a>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="clear: both; padding-top:15px;"/>
                                            <div>
                                                Если Вы хотите изменить количество товаров, введите нужное Вам количество в соответствующей строке и нажмите кнопку «Пересчитать».
                                                Для того, чтобы удалить ненужный товар, нажмите на красный крестик в соответствующей строке.
                                                После того как заказ будет сформирован, нажмите кнопку «Оформить заказ».
                                            </div>
                                            <div style="clear: both"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div align="center" style="padding: 100px 0px 300px" class="content">
                                        Корзина пуста, отправляйтесь в каталог за покупками!
                                    </div>
                                    <!--xsl:apply-templates select="/DOCUMENT/CONTENT"/-->
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>