<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template name="faq">
        <div class="row">
            <div class="col-md-2"/>
            <div class="col-md-8">
                <xsl:if test="//INCLUDE[@name='Faq']/MSG">
                    <div class="bg-danger" style="margin: 20px auto; padding: 20px; border-radius: 5px">
                        <xsl:value-of select="//INCLUDE[@name='Faq']/MSG"/>
                    </div>
                </xsl:if>
                <form method="post" action="/{//REALM/@name}" name="faq" id="faq" role="form">
                    <div class="form-group">
                        <label for="name">Ваше имя:</label>
                        <input type="name" class="form-control" id="name" name="faq[quser]"/>
                    </div>
                    <div class="form-group">
                        <label for="mail">Ваш e-mail:</label>
                        <input type="mail" class="form-control" id="mail" name="faq[email]"/>
                    </div>
                    <div class="form-group">
                        <label for="msg">Ваш вопрос:</label>
                        <textarea type="msg" rows="3" name="faq[question]" id="msg" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <img src="/captcha.php"/>
                        <input type="hidden" name="cchash" value="{//SESSION/@ch}" style="vertical-align:top"/>
                        <label for="cc">&#xA0;введите код с картинки:</label>
                        <input type="text" name="cc" id="cc" class="form-control" style="display: inline-block; width: 80px;vertical-align:top;margin-left:5px;margin-top:0;"/>
                    </div>
                    <div align="right">
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>