<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="RESULT">
        <div style="margin: 10px 0px; color: #BCBCBC" class="search-res">
            <xsl:variable name="parent" select="@realm"/>
            <div><a href="/{//SECTION[@realm=$parent]/@name}?node={@node}" style="font-size: 15px; color: #fff; font-weight: bold"><xsl:value-of select="//SECTION[@realm=$parent]/@title"/></a></div>
            <xsl:value-of select="."/>...<a href="/{//SECTION[@realm=$parent]/@name}?node={@node}">перейти</a>
        </div>
    </xsl:template>
    <xsl:template name="sitemap">
        <div class="sitemap">
            <xsl:variable name="lang" select="substring(/DOCUMENT/REALM/@name,1,2)"/>
            <xsl:variable name="root" select="//SECTION[@name=$lang]/@realm"/>
            <xsl:for-each select="//SECTION[@parent=$root]">
                <div class="sub"> • <a href="/{@name}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a>
                    <xsl:for-each select="SECTION">
                        <div class="sub"> • <a href="/{@name}"><xsl:value-of select="@title" disable-output-escaping="yes" /></a>
                            <xsl:for-each select="SECTION">
                                <div class="sub"> • <a href="/{@name}"><xsl:value-of select="@title" disable-output-escaping="yes" /></a>
                                    <xsl:for-each select="SECTION">
                                        <div class="sub"> • <a href="/{@name}"><xsl:value-of select="@title" disable-output-escaping="yes" /></a>
                                            <xsl:for-each select="SECTION">
                                                <div class="sub"> • <a href="/{@name}"><xsl:value-of select="@title" disable-output-escaping="yes" /></a></div>
                                            </xsl:for-each>
                                        </div>
                                    </xsl:for-each>
                                </div>
                            </xsl:for-each>
                        </div>
                    </xsl:for-each>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>
    <xsl:template name="way">
        <xsl:variable name="realm" select="//REALM/@id"/>
        <div class="way">
            <a href="/">Главная</a>
            <xsl:for-each select="//SECTION[contains(//REALM/@name, @name) and @realm != 1000]">
                <xsl:choose>
                    <xsl:when test="@realm = $realm and not(//PARAM[@name='node'])">
                        /&#xA0;&#xA0;<b><xsl:value-of select="@title"/></b>
                    </xsl:when>
                    <xsl:otherwise>
                        /&#xA0;&#xA0;<a href="/{@name}"><xsl:value-of select="@title"/></a>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:if test="//PARAM[@name='node']">
                <xsl:variable name="node" select="//PARAM[@name='node']"/>
                /&#xA0;<b><a href="/{//SECTION[@realm = $realm]/@name}?node={$node}"><xsl:value-of select="//NODE[@node=$node]/@name"/></a></b>
            </xsl:if>
        </div>
    </xsl:template>
    <xsl:template match="PAGING">
        <div class="paging-container">
            <xsl:variable name="page" select="@page"/>
            <xsl:variable name="realm" select="//REALM/@name"/>
            <xsl:variable name="params" select="count(//PARAMS[@source='get']/PARAM)"/>
            <xsl:variable name="paramsmass" select="count(//PARAMS[@source='get']/PARAMSET/PARAM)"/>
            <xsl:variable name="param">&amp;<xsl:for-each select="//PARAMS[@source='get']/PARAM[@name != 'page']"><xsl:value-of select="@name"/>=<xsl:value-of select="."/>&amp;</xsl:for-each><xsl:for-each select="//PARAMS[@source='get']/PARAMSET/PARAM"><xsl:value-of select="../@name"/>[<xsl:value-of select="@name"/>]=<xsl:value-of select="."/>&amp;</xsl:for-each></xsl:variable>
            <!--xsl:variable name="param">&amp;
                <xsl:for-each select="//PARAMS[@source='get']/PARAM[@name != 'page']">
                    <xsl:value-of select="@name"/>=<xsl:value-of select="."/>
                    <xsl:if test="not(position() = $params)">&amp;</xsl:if>
                </xsl:for-each>
                <xsl:for-each select="//PARAMS[@source='get']/PARAMSET/PARAM">
                    <xsl:value-of select="../@name"/>[<xsl:value-of select="@name"/>]=<xsl:value-of select="."/>
                    <xsl:if test="not(position() = $paramsmass)">&amp;</xsl:if>
                </xsl:for-each>
            </xsl:variable-->
            <div onclick="window.location.href='/{$realm}?page={FIRST/@page}{$param}';" class="paging-button">
                <xsl:if test="not(FIRST)"><xsl:attribute name="style">display: none</xsl:attribute></xsl:if>
            </div>
            <div onclick="window.location.href='/{$realm}?page={PREV/@page}{$param}';" class="paging-button">
                <xsl:if test="not(PREV)"><xsl:attribute name="style">display: none</xsl:attribute></xsl:if>
            </div>
            <xsl:for-each select="PAGE">
                <xsl:if test="@number = $page">&#xA0;<b class="page"><xsl:value-of select="@number"/></b>&#xA0;</xsl:if>
                <xsl:if test="not(@number = $page)">&#xA0;<a class="page" href="/{$realm}?page={@number}{$param}"><xsl:value-of select="@number"/></a>&#xA0;</xsl:if>
            </xsl:for-each>
            <!--&#xA0;из&#xA0;<xsl:value-of select="@pages"/-->

            <div onclick="window.location.href='/{$realm}?page={NEXT/@page}{$param}';" class="paging-button">
                <xsl:if test="not(NEXT)"><xsl:attribute name="style">display: none</xsl:attribute></xsl:if>
            </div>
            <div onclick="window.location.href='/{$realm}?page={LAST/@page}{$param}';" class="paging-button">
                <xsl:if test="not(LAST)"><xsl:attribute name="style">display: none</xsl:attribute></xsl:if>
            </div>
        </div>
    </xsl:template>
    <xsl:template name="form-registration">
        <div align="center">
            <form name="reg" id="reg" method="post">
                <xsl:if test="//INCLUDE[@name='User']/MSG">
                    <div class="msg">
                        <xsl:value-of select="//INCLUDE[@name='User']/MSG"/>
                    </div>
                </xsl:if>
                <table style="margin-top: 10px">
                    <tbody>
                        <tr>
                            <td valign="top"><B>Электронная почта*</B></td>
                            <td><input type="text" name="email" id="email" class="reg-form bold" value="{//PARAMS[@source='post']/PARAM[@name='email']}"/>
                                <input type="hidden" name="registration" value="yes"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><B>Пароль*</B></td>
                            <td><input type="password" name="pass" id="pass" class="reg-form bold"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><B>Повторите пароль*</B></td>
                            <td><input type="password" name="pass2" id="pass2" class="reg-form bold"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><B>Фамилия Имя Отчество*</B></td>
                            <td valign="top"><input type="text" name="name" id="name" class="reg-form bold" value="{//PARAMS[@source='post']/PARAM[@name='name']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Компания</td>
                            <td><input type="text" name="company" id="company" class="reg-form" value="{//PARAMS[@source='post']/PARAM[@name='company']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><B>Телефон*</B></td>
                            <td><input type="text" name="phone" id="phone" class="reg-form bold" value="{//PARAMS[@source='post']/PARAM[@name='phone']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table cellpadding="0px" cellspacing="4px" border="0px" width="100%">
                                    <tr>
                                        <td align="right" width="50%">
                                            <img src="/captcha.php"/>
                                            <input type="hidden" name="cchash" value="{//SESSION/@ch}"/>
                                        </td>
                                        <td align="left" width="50%">
                                            <input type="text" name="cc" style="width: 60px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">введите код с картинки</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <div class="cabinet-btn" onclick="reg.submit();">Зарегистрироваться</div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                Поля отмеченные * - обязательны для заполнения
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </xsl:template>
    <xsl:template name="profile-edit">
        <div class="block">
            <div class="title">
                <xsl:value-of select="//REALM/TITLE"/>
            </div>
            <div class="content">
                <form name="profileedit" id="profileedit" method="post" enctype="multipart/form-data">
                    <xsl:if test="//INCLUDE[@name='User']/MSG">
                        <div class="msg">
                            <xsl:value-of select="//INCLUDE[@name='User']/MSG"/>
                        </div>
                    </xsl:if>
                    <table style="margin-top: 10px">
                        <tr>
                            <td valign="top" collspan="2">
                                <a href="/cabinet/orders"><div class="cabinet-btn">Отслеживание заказов</div></a>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Логин (латиницей)
                            </td>
                            <td>
                                <input type="text" name="login" id="userlogin" class="reg-form" value="{//SESSION/USER/@login}" disabled="disabled"/>
                                <input type="hidden" name="userlogin" value="{//SESSION/USER/@login}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Пароль
                            </td>
                            <td><input type="password" name="pass" id="pass" class="reg-form"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Повторите пароль
                            </td>
                            <td><input type="password" name="pass2" id="pass2" class="reg-form"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Фамилия Имя Отчество
                            </td>
                            <td valign="top"><input type="text" name="name" id="name" class="reg-form" value="{//USER/FIELD[@name='name']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><B>ИИН/БИН*</B></td>
                            <td valign="top"><input type="text" name="bin" id="bin" class="reg-form" value="{//USER/FIELD[@name='bin']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Компания
                            </td>
                            <td valign="top"><input type="text" name="company" id="company" class="reg-form" value="{//USER/FIELD[@name='company']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Телефон
                            </td>
                            <td><input type="text" name="phone" id="phone" class="reg-form" value="{//USER/FIELD[@name='phone']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Электронная почта
                            </td>
                            <td><input type="text" name="email" id="email" class="reg-form" value="{//USER/FIELD[@name='email']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2" align="center" style="padding: 5px"><B>Адрес доставки</B>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Индекс
                            </td>
                            <td>
                                <input type="text" name="postindex" id="postindex" class="reg-form" value="{//USER/FIELD[@name='postindex']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Страна
                            </td>
                            <td>
                                <input type="text" name="country" id="country" class="reg-form"  value="{//USER/FIELD[@name='country']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Область
                            </td>
                            <td>
                                <input type="text" name="obl" id="obl" class="reg-form" value="{//USER/FIELD[@name='obl']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Город
                            </td>
                            <td>
                                <input type="text" name="city" id="city" class="reg-form" value="{//USER/FIELD[@name='city']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Улица
                            </td>
                            <td>
                                <input type="text" name="street" id="street" class="reg-form" value="{//USER/FIELD[@name='street']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Дом
                            </td>
                            <td>
                                <input type="text" name="build" id="build" class="reg-form" value="{//USER/FIELD[@name='build']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Квартира
                            </td>
                            <td>
                                <input type="text" name="app" id="app" class="reg-form" value="{//USER/FIELD[@name='app']}"/>
                                <div/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="cabinet-btn" onclick="profileedit.submit()">Сохранить</div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="SECTION[@weight &gt; 0]">
        <div class="left-menu-item">
            <div class="wrapper">
                <xsl:choose>
                    <xsl:when test="SECTION[@weight &gt; 0]">
                        <xsl:choose>
                            <xsl:when test="contains(//REALM/@name, @name)">
                                <div onclick="UnHide(this, {@realm})" class="arrow"><img src="/i/bullet-down.png"/></div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div onclick="UnHide(this, {@realm})" class="arrow"><img src="/i/bullet.png"/></div>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="arrow" style="color: #FFFFA3; cursor: default;">
                            <img src="/i/x.gif"/>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
                <div style="float: left; margin-top: 3px; max-width: 95%">
                    <xsl:choose>
                        <xsl:when test="SECTION[@weight &gt; 0]">
                            <a href="/{@name}" onclick="UnHidel(this, {@realm}); return false"><xsl:value-of select="@title"/></a>
                        </xsl:when>
                        <xsl:otherwise>
                            <a href="/{@name}"><xsl:value-of select="@title"/></a>
                            <!--xsl:choose>
                                <xsl:when test="@parent='1267'">
                                    <a href="/{@name}" class="mnulink"><xsl:value-of select="@title"/></a>
                                </xsl:when>
                                <xsl:when test="@parent='1310'">
                                    <a href="/{@name}" class="mnulink"><xsl:value-of select="@title"/></a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <a href="/{@name}" class="mnulink_"><xsl:value-of select="@title"/></a>
                                </xsl:otherwise>
                            </xsl:choose-->
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
                <div style="clear: both;"/>
            </div>
            <xsl:if test="SECTION[@weight &gt; 0]">
                <div id="{@realm}" align="left" style="padding: 0px 10px; clear: both;">
                    <xsl:choose>
                        <xsl:when test="contains(//REALM/@name, @name)">
                            <xsl:attribute name="class"></xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class">cl</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="SECTION"/>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    <xsl:template match="ERROR">
        <div class="error">
            <xsl:value-of select="."/>(error code <xsl:value-of select="@code"/>)
        </div>
    </xsl:template>
</xsl:stylesheet>