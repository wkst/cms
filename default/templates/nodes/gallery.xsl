<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="NODE[@class='gallery']">
        1111<a class="gallery-img" rel="shadowbox[scr]" href="{DATATYPE/FIELD[@name='image1']/IMAGE[@prefix='full']}" style="background:url({DATATYPE/FIELD[@name='image1']/IMAGE[@prefix='prev']}) no-repeat 0 0 /  cover;"/>    
    </xsl:template>
</xsl:stylesheet>