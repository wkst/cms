<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="NODE[@class='article']">       
        <!--div class="about_simple_news">
            <xsl:variable name="realm" select="@realm"/>
            <a class="img" href="/{//SECTION[@realm=$realm]/@name}?node={@node}" style="background: url({DATATYPE/FIELD[@name='image']/IMAGE[@prefix = 'prev']}) repeat scroll center top / cover rgba(0, 0, 0, 0);"></a>
            <div class="title">
                <a href="/{//SECTION[@realm=$realm]/@name}?node={@node}"><xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/></a>
            </div>
            <div class="date">
                <xsl:value-of select="substring(DATATYPE/FIELD[@name='datetime']/DATA,1,10)" disable-output-escaping="yes"/>    
            </div>
            <div class="content" align="left">
                <xsl:value-of select="DATATYPE/FIELD[@name='brief']/DATA" disable-output-escaping="yes"/>...
                <a href="/{//SECTION[@realm=$realm]/@name}?node={@node}" class="link">подробнее &gt;&gt;&gt;</a>  
            </div>
            <div style="clear:both"></div>
        </div-->
        <xsl:variable name="rlm" select="@realm"/>
        <div class="main-news-item">
            <div class="img">
                <xsl:choose>
                    <xsl:when test="DATATYPE/FIELD[@name='image']/DATA != ''">
                        <img src="/i/x.gif" width="90px" height="60px" style="background-image: url({DATATYPE/FIELD[@name='image']/IMAGE[@prefix='prev']}); background-size: contain"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <img src="/i/x.gif" width="90px" height="60px" style="background-image: url(/img/no-image.png); background-size: 70%; background-repeat: no-repeat; background-position: center"/>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
            <div class="title">
                <a href="/{//SECTION[@realm=$rlm]/@name}?node={@node}"><xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/></a>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="DATATYPE[@name='article']">
        <div class="block">
            <xsl:if test="//PARAM[@name='node']">
                <div class="title">
                    <xsl:value-of select="FIELD[@name='title']/DATA"/>
                </div>
            </xsl:if>
            <div class="content" style="padding-right: 20px">
                <xsl:value-of select="FIELD[@name='content']/DATA" disable-output-escaping="yes"/>
            </div>
            <div align="right" style="padding: 10px 20px 20px 0px">
                <xsl:variable name="realm" select="//REALM/@id"/>
                <a href="/{//SECTION[@realm=$realm]/@name}" class="link">Назад</a>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>