<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="NODE[DATATYPE[@class='catalog']]">
        <div class="node-container" align="left">
            <xsl:choose>
                <xsl:when test="@highlight != ''">
                    <xsl:attribute name="class">node-container-width highlight catalog-container</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class">node-container-width catalog-container</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="DATATYPE/FIELD[@name='otmetka']/DATA = 239">
                    <div style="position:absolute; margin-top:-5px; margin-left:353px;" title="Рекомендуем!"><img src="/i/recomen.png"/></div>
                </xsl:when>
                <xsl:when test="DATATYPE/FIELD[@name='otmetka']/DATA = 240">
                    <div style="position:absolute; margin-top:-5px; margin-left:353px;" title="Хит продаж!"><img src="/i/hit.png"/></div>
                </xsl:when>
                <xsl:when test="DATATYPE/FIELD[@name='otmetka']/DATA = 241">
                    <div style="position:absolute; margin-top:-5px; margin-left:353px;" title="Скидка!"><img src="/i/skidka.png"/></div>
                </xsl:when>
                <xsl:when test="DATATYPE/FIELD[@name='otmetka']/DATA = 242">
                    <div style="position:absolute; margin-top:-5px; margin-left:353px;" title="Новинка!"><img src="/i/new.png"/></div>
                </xsl:when>
            </xsl:choose>
            <table width="100%" border="0px" style="margin-bottom: -10px; clear:both;">
                <xsl:variable name="realm" select="//REALM/@id"/>
                <tr>
                    <td valign="top" rowspan="2" width="100px">
                        <a href="/{//SECTION[@realm = $realm]/@name}?node={@node}">
                        <xsl:choose>
                            <xsl:when test="DATATYPE/FIELD[@name='image1']/DATA != ''">
                                <div class="catalog-image"><img src="/i/x.gif" width="135px" height="95px" style="background-size: contain; background-image: url(/files/{@node}/small_{DATATYPE/FIELD[@name='image1']/DATA}); background-repeat: no-repeat; background-position: center;"/></div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="catalog-image"><img src="/i/no_image.jpg" width="100px"/></div>
                            </xsl:otherwise>
                        </xsl:choose>
                        </a>
                    </td>
                    <td valign="top" class="catalog-title" width="55%">
                        <div>
                            <xsl:if test="DATATYPE/FIELD[@name='otmetka']/DATA = 239 or DATATYPE/FIELD[@name='otmetka']/DATA = 240 or DATATYPE/FIELD[@name='otmetka']/DATA = 241 or DATATYPE/FIELD[@name='otmetka']/DATA = 242">
                                <xsl:attribute name="style">width: 204px;</xsl:attribute>
                            </xsl:if>
                            <a href="/{//SECTION[@realm = $realm]/@name}?node={@node}">
                                <xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/>
                            </a>
                        </div>
                    </td>
                    <!--td class="catalog-cost" rowspan="2" align="center">
                        <div style="float: right"><xsl:value-of select="DATATYPE/FIELD[@name='cost']/COST"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='cost']/@unit"/></div>
                    </td>
                    <td rowspan="2" width="90px" align="center">
                        <input type="checkbox" name="{DATATYPE/@name}" class="compare-check" id="{@node}" onchange="SetCompare(this)">
                            <xsl:if test="//PARAMSET[@name='compare']/PARAM = @node">
                                <xsl:attribute name="checked">checked</xsl:attribute>
                            </xsl:if>
                        </input>
                    </td-->
                </tr>
                <xsl:choose>
                    <xsl:when test="DATATYPE/@name='projector'">
                        <tr>
                            <td valign="top">
                            <B>яркость:</B>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='lightness_manual']/DATA"/><br/>
                            <b>контрастность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='contrast']/DATA"/><br/>
                            <b>разрешение:</b>&#xA0;
                                <xsl:variable name="list" select="DATATYPE/FIELD[@name='main_res']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                            <b>срок службы лампы:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='lamptime']/DATA"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='amplifier'">
                        <tr>
                            <td valign="top">
                            <b>Суммарная мощность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sumpower']/DATA"/><br/>
                            <b>Входы:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='in']/DATA"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='homecinema'">
                        <tr>
                            <td valign="top">
                            <b>Комплектация:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='complectation']/DATA"/><br/>
                            </td>
                        </tr>
                    </xsl:when> 
                    <xsl:when test="DATATYPE/@name='acousticsystem'">
                        <tr>
                            <td valign="top">
                            <b>Система:</b>&#xA0;
                                <xsl:variable name="list" select="DATATYPE/FIELD[@name='system']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                            <b>Мощность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='amplify']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='amplify']/@unit"/><br/>
                            <b>Диапазон частот:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='fqlimit']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='fqlimit']/@unit"/><br/>
                            <b>Чувствительность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensivity']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensivity']/@unit"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='complect'">
                        <tr>
                            <td valign="top">
                            <b>Комплектация:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='complectation']/DATA"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='gromkogovoritel'">
                        <tr>
                            <td valign="top">
                            <b>Мощность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='musicpower']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='musicpower']/@unit"/><br/>
                            <b>Диапазон частот:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='freqdiapazon']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='freqdiapazon']/@unit"/><br/>
                            <b>Чувствительность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensivity']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensivity']/@unit"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='accessories'">
                        <tr>
                            <td valign="top">
                            <b>Тип комплекта:</b>&#xA0;
                                <xsl:variable name="list" select="DATATYPE/FIELD[@name='acctype']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='headphones'">
                        <tr>
                            <td valign="top">
                            <b>Мощность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='amplify']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='amplify']/@unit"/><br/>
                            <b>Диапазон частот:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='fqlimit']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='fqlimit']/@unit"/><br/>
                            <b>Чувствительность:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensivity']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensivity']/@unit"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='blueray'">
                        <tr>
                            <td valign="top">
                            <b>Входы:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='ins']/DATA"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='proectdisplay'">
                        <tr>
                            <td valign="top">
                            <b>Тип привода:</b>&#xA0;
                                <xsl:variable name="list" select="DATATYPE/FIELD[@name='privod_type']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                            <b>Способ крепления:</b>&#xA0;
                                <xsl:variable name="list1" select="DATATYPE/FIELD[@name='sposob_krepl']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list1]/@value"/><br/>
                            <b>Соотношение сторон:</b>&#xA0;
                                <xsl:variable name="list2" select="DATATYPE/FIELD[@name='sootn_storon']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list2]/@value"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='doccamera'">
                        <tr>
                            <td valign="top">
                            <b>Сенсор цветных изображений:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='sensor_cvetnih_izobr']/DATA"/><br/>
                            <b>Фокусировка:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='camfocus']/DATA"/><br/>
                            <b>Увеличение:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='uvelichenie']/DATA"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='interactiveclass'">
                        <tr>
                            <td valign="top">
                            <b>Диагональ:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diagonal']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diagonal']/@unit"/><br/>
                            <b>Принцип работы:</b>&#xA0;
                                <xsl:variable name="list" select="DATATYPE/FIELD[@name='workprincip']/DATA"/>
                                <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='ebook'">
                        <tr>
                            <td valign="top">
                            <b>Диcплей:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='display']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='display']/@unit"/><br/>
                            <b>Диагональ:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diagonal']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diagonal']/@unit"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='kreplenie'">
                        <tr>
                            <td valign="top">
                            <b>Максимальная нагрузка:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='max_weight']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='max_weight']/@unit"/><br/>
                            <xsl:if test="DATATYPE/FIELD[@name='diag_from']/DATA != '0'">
                                <b>Диагональ от:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diag_from']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diag_from']/@unit"/><br/>
                            </xsl:if>
                            <xsl:if test="DATATYPE/FIELD[@name='diag_to']/DATA != '0'">
                                <b>Диагональ до:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diag_to']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diag_to']/@unit"/><br/>
                            </xsl:if>
                            </td>
                        </tr>
                    </xsl:when>
                    <xsl:when test="DATATYPE/@name='tvtumba'">
                        <tr>
                            <td valign="top">
                            <b>Диагональ устанавливаемых ТВ-панелей:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diagonal_ust_TV_panelei']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='diagonal_ust_TV_panelei']/@unit"/><br/>
                            <b>Выдерживаемый вес кронштейнов для ТВ:</b>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='weight_for_TV']/DATA"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='weight_for_TV']/@unit"/><br/>
                            </td>
                        </tr>
                    </xsl:when>
                </xsl:choose>
                <tr>
                    <td colspan="4" style="padding: 5px; font-style:italic; height: 80px; overflow: hidden;">
                        <div style="height: 51px; width: 270px; overflow: hidden;">
                            <!--xsl:if test="DATATYPE/@name='3D' or DATATYPE/@name='accessories' or DATATYPE/@name='accessories' or DATATYPE/@name='antivirus' or DATATYPE/@name='cabel' or DATATYPE/@name='complect' or DATATYPE/@name='homecinema' or DATATYPE/@name='printtech' or DATATYPE/@name='subwoofer'">
                                <xsl:attribute name="style">overflow: hidden;</xsl:attribute>
                            </xsl:if-->
                            <xsl:value-of select="DATATYPE/FIELD[@name='review']/BRIEF" disable-output-escaping="yes"/>...<!--a href="/{//SECTION[@realm = $realm]/@name}?node={@node}" class="link_bold">подробнее&gt;&gt;</a><BR/-->
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%" style="border-spacing: 0px;" border="0px">
                <tr>
                    <td style="padding-left:10px;" width="50%">
                        <xsl:if test="DATATYPE/FIELD[@name='price']/FIELD[@name='price1'] != '99999999'">
                            <div class="btn-cart cartadd" id="{@node}">
                                <input type="hidden" class="cart_product" name="cart_product" value="{@node}"/>
                                <div style="text-decoration:underline; background:url('../i/korzina_small.jpg') no-repeat; background-position: left; padding-left:30px;">Добавить в корзину</div>
                            </div> 
                            <!--div class="btn-cart" onclick="$('#{@node}').dialog('open')">
                                <div style="text-decoration:underline; background:url('../i/korzina_small.jpg') no-repeat; background-position: left; padding-left:30px;">Добавить в корзину</div>
                            </div>        
                            <div class="cart-dialog" id="{@node}">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            Добавить в корзину выбранный товар:<br/>
                                            <xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/>?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%" align="right">
                                            Количество:
                                        </td>
                                        <td width="50%">
                                            <input type="hidden" class="cart_product" name="cart_product" value="{@node}"/>
                                            <input type="text" class="cart_count" name="cart_count" value="1" style="width: 35px"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <div class="cartadd">Добавить</div>
                                        </td>
                                    </tr>
                                </table>
                            </div-->
                        </xsl:if>
                    </td>
                    <td>
                        <xsl:if test="DATATYPE/FIELD[@name='cost']/DATA!=''">
                            <div class="old_cost"><xsl:value-of select="format-number(DATATYPE/FIELD[@name='cost']/DATA, '### ##0.00', 'my')"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='cost']/@unit"/></div>
                        </xsl:if>
                        <div class="catalog-cost">
                            <xsl:choose>
                                <xsl:when test="DATATYPE/FIELD[@name='price']/FIELD[@name='price1'] != '99999999'">
                                    <div style="text-align:center;"><xsl:value-of select="format-number(DATATYPE/FIELD[@name='price']/FIELD[@name='price1'], '### ##0', 'my')"/>&#xA0;<xsl:value-of select="DATATYPE/FIELD[@name='cost']/@unit"/></div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div style="text-align:center;">Цену уточняйте</div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                        
                    </td>
                </tr>
            </table>
        </div>
    </xsl:template>
    <xsl:template match="DATATYPE[@class='catalog']">
        <table cellpadding="0px" cellspacing="0px" width="100%" style="margin-top:15px; border-radius:5px; border:1px solid #c5c3c3; padding:5px 7px;">
            <tr>
                <td style="width: 450px; padding-top: 5px; padding-left: 5px" valign="top">
                    <div id="pre">
                        <xsl:if test="FIELD[@name='image1']/DATA != ''">
                            <div id="pre1" class="images-big-preview" style="display: block"><a href="/files/{../@node}/{FIELD[@name='image1']/DATA}"><img id="preview" src="/files/{../@node}/{FIELD[@name='image1']/DATA}" width="430px" style="border:1px solid #e3e2e1;"/></a></div>
                        </xsl:if>
                        <xsl:if test="FIELD[@name='image2']/DATA != ''">
                            <div id="pre2" class="images-big-preview"><a href="/files/{../@node}/{FIELD[@name='image2']/DATA}"><img id="preview" src="/files/{../@node}/{FIELD[@name='image2']/DATA}" width="430px" style="border:1px solid #e3e2e1;"/></a></div>
                        </xsl:if>
                        <xsl:if test="FIELD[@name='image3']/DATA != ''">
                            <div id="pre3" class="images-big-preview"><a href="/files/{../@node}/{FIELD[@name='image3']/DATA}"><img id="preview" src="/files/{../@node}/{FIELD[@name='image3']/DATA}" width="430px" style="border:1px solid #e3e2e1;"/></a></div>
                        </xsl:if>
                        <xsl:if test="FIELD[@name='image4']/DATA != ''">
                            <div id="pre4" class="images-big-preview"><a href="/files/{../@node}/{FIELD[@name='image4']/DATA}"><img id="preview" src="/files/{../@node}/{FIELD[@name='image4']/DATA}" width="430px" style="border:1px solid #e3e2e1;"/></a></div>
                        </xsl:if>
                    </div>
                    <div class="images" id="imgbox" valign="top">
                        <xsl:if test="FIELD[@name='image1']/DATA != ''">
                            <div class="images-preview" onmouseover="switchprev('pre1')"><img src="/files/{../@node}/small_{FIELD[@name='image1']/DATA}" width="110px"/></div>
                        </xsl:if>
                        <xsl:if test="FIELD[@name='image2']/DATA != ''">
                            <div class="images-preview" onmouseover="switchprev('pre2')"><img src="/files/{../@node}/small_{FIELD[@name='image2']/DATA}" width="110px"/></div>
                        </xsl:if>
                        <xsl:if test="FIELD[@name='image3']/DATA != ''">
                            <div class="images-preview" onmouseover="switchprev('pre3')"><img src="/files/{../@node}/small_{FIELD[@name='image3']/DATA}" width="110px"/></div>
                        </xsl:if>
                        <xsl:if test="FIELD[@name='image4']/DATA != ''">
                            <div class="images-preview" onmouseover="switchprev('pre4')"><img src="/files/{../@node}/small_{FIELD[@name='image4']/DATA}" width="110px"/></div>
                        </xsl:if>
                    </div>
                </td>
                <td valign="top" style="padding-top: 5px; font-size:13px;">
                    <xsl:if test="FIELD[@name='otmetka']/DATA = 239">
                        <div style="position:absolute; margin-top:-10px; margin-left:362px;" title="Рекомендуем!"><img src="/i/recomen.png"/></div>
                    </xsl:if>
                    <xsl:if test="FIELD[@name='otmetka']/DATA = 240">
                        <div style="position:absolute; margin-top:-10px; margin-left:362px;" title="Хит продаж!"><img src="/i/hit.png"/></div>
                    </xsl:if>
                    <xsl:if test="FIELD[@name='otmetka']/DATA = 241">
                        <div style="position:absolute; margin-top:-10px; margin-left:362px;" title="Скидка!"><img src="/i/skidka.png"/></div>
                    </xsl:if>
                    <xsl:if test="FIELD[@name='otmetka']/DATA = 242">
                        <div style="position:absolute; margin-top:-10px; margin-left:362px;" title="Новинка!"><img src="/i/new.png"/></div>
                    </xsl:if>
                    <div class="node-title catalog-title" style="clear: both; float: none;">
                        <xsl:if test="FIELD[@name='otmetka']/DATA = 239 or FIELD[@name='otmetka']/DATA = 240 or FIELD[@name='otmetka']/DATA = 241 or FIELD[@name='otmetka']/DATA = 242">
                            <xsl:attribute name="style">width: 365px; float: none;</xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="FIELD[@name='title']/DATA"/>
                    </div>
                    <table style="border-spacing:0px; margin-bottom:20px; margin-right:10px;">
                        <xsl:choose>
                            <xsl:when test="../DATATYPE/@name='projector'">
                                <tr>
                                    <td valign="top">
                                    <B>Яркость:</B>&#xA0;<xsl:value-of select="FIELD[@name='lightness_manual']/DATA"/><br/>
                                    <b>Контрастность:</b>&#xA0;<xsl:value-of select="FIELD[@name='contrast']/DATA"/><br/>
                                    <b>Разрешение:</b>&#xA0;
                                        <xsl:variable name="list" select="FIELD[@name='main_res']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                                    <b>Срок службы лампы:</b>&#xA0;<xsl:value-of select="FIELD[@name='lamptime']/DATA"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='amplifier'">
                                <tr>
                                    <td valign="top">
                                    <b>Суммарная мощность:</b>&#xA0;<xsl:value-of select="FIELD[@name='sumpower']/DATA"/><br/>
                                    <b>Входы:</b>&#xA0;<xsl:value-of select="FIELD[@name='in']/DATA"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='homecinema'">
                                <tr>
                                    <td valign="top">
                                    <b>Комплектация:</b>&#xA0;<xsl:value-of select="FIELD[@name='complectation']/DATA"/><br/>
                                    </td>
                                </tr>
                            </xsl:when> 
                            <xsl:when test="../DATATYPE/@name='acousticsystem'">
                                <tr>
                                    <td valign="top">
                                    <b>Система:</b>&#xA0;
                                        <xsl:variable name="list" select="FIELD[@name='system']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                                    <b>Мощность:</b>&#xA0;<xsl:value-of select="FIELD[@name='amplify']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='amplify']/@unit"/><br/>
                                    <b>Диапазон частот:</b>&#xA0;<xsl:value-of select="FIELD[@name='fqlimit']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='fqlimit']/@unit"/><br/>
                                    <b>Чувствительность:</b>&#xA0;<xsl:value-of select="FIELD[@name='sensivity']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='sensivity']/@unit"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='complect'">
                                <tr>
                                    <td valign="top">
                                    <b>Комплектация:</b>&#xA0;<xsl:value-of select="FIELD[@name='complectation']/DATA"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='gromkogovoritel'">
                                <tr>
                                    <td valign="top">
                                    <b>Мощность:</b>&#xA0;<xsl:value-of select="FIELD[@name='musicpower']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='musicpower']/@unit"/><br/>
                                    <b>Диапазон частот:</b>&#xA0;<xsl:value-of select="FIELD[@name='freqdiapazon']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='freqdiapazon']/@unit"/><br/>
                                    <b>Чувствительность:</b>&#xA0;<xsl:value-of select="FIELD[@name='sensivity']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='sensivity']/@unit"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='accessories'">
                                <tr>
                                    <td valign="top">
                                    <b>Тип комплекта:</b>&#xA0;
                                        <xsl:variable name="list" select="FIELD[@name='acctype']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='headphones'">
                                <tr>
                                    <td valign="top">
                                    <b>Мощность:</b>&#xA0;<xsl:value-of select="FIELD[@name='amplify']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='amplify']/@unit"/><br/>
                                    <b>Диапазон частот:</b>&#xA0;<xsl:value-of select="FIELD[@name='fqlimit']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='fqlimit']/@unit"/><br/>
                                    <b>Чувствительность:</b>&#xA0;<xsl:value-of select="FIELD[@name='sensivity']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='sensivity']/@unit"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='blueray'">
                                <tr>
                                    <td valign="top">
                                    <b>Входы:</b>&#xA0;<xsl:value-of select="FIELD[@name='ins']/DATA"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='proectdisplay'">
                                <tr>
                                    <td valign="top">
                                    <b>Тип привода:</b>&#xA0;
                                        <xsl:variable name="list" select="FIELD[@name='privod_type']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                                    <b>Способ крепления:</b>&#xA0;
                                        <xsl:variable name="list1" select="FIELD[@name='sposob_krepl']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list1]/@value"/><br/>
                                    <b>Соотношение сторон:</b>&#xA0;
                                        <xsl:variable name="list2" select="FIELD[@name='sootn_storon']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list2]/@value"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='doccamera'">
                                <tr>
                                    <td valign="top">
                                    <b>Сенсор цветных изображений:</b>&#xA0;<xsl:value-of select="FIELD[@name='sensor_cvetnih_izobr']/DATA"/><br/>
                                    <b>Фокусировка:</b>&#xA0;<xsl:value-of select="FIELD[@name='camfocus']/DATA"/><br/>
                                    <b>Увеличение:</b>&#xA0;<xsl:value-of select="FIELD[@name='uvelichenie']/DATA"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='interactiveclass'">
                                <tr>
                                    <td valign="top">
                                    <b>Диагональ:</b>&#xA0;<xsl:value-of select="FIELD[@name='diagonal']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='diagonal']/@unit"/><br/>
                                    <b>Принцип работы:</b>&#xA0;
                                        <xsl:variable name="list" select="FIELD[@name='workprincip']/DATA"/>
                                        <xsl:value-of select="//LIST[@id=$list]/@value"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='ebook'">
                                <tr>
                                    <td valign="top">
                                    <b>Диcплей:</b>&#xA0;<xsl:value-of select="FIELD[@name='display']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='display']/@unit"/><br/>
                                    <b>Диагональ:</b>&#xA0;<xsl:value-of select="FIELD[@name='diagonal']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='diagonal']/@unit"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='kreplenie'">
                                <tr>
                                    <td valign="top">
                                    <b>Максимальная нагрузка:</b>&#xA0;<xsl:value-of select="FIELD[@name='max_weight']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='max_weight']/@unit"/><br/>
                                    <xsl:if test="FIELD[@name='diag_from']/DATA != '0'">
                                        <b>Диагональ от:</b>&#xA0;<xsl:value-of select="FIELD[@name='diag_from']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='diag_from']/@unit"/><br/>
                                    </xsl:if>
                                    <xsl:if test="FIELD[@name='diag_to']/DATA != '0'">
                                        <b>Диагональ до:</b>&#xA0;<xsl:value-of select="FIELD[@name='diag_to']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='diag_to']/@unit"/><br/>
                                    </xsl:if>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test="../DATATYPE/@name='tvtumba'">
                                <tr>
                                    <td valign="top">
                                    <b>Диагональ устанавливаемых ТВ-панелей:</b>&#xA0;<xsl:value-of select="FIELD[@name='diagonal_ust_TV_panelei']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='diagonal_ust_TV_panelei']/@unit"/><br/>
                                    <b>Выдерживаемый вес кронштейнов для ТВ:</b>&#xA0;<xsl:value-of select="FIELD[@name='weight_for_TV']/DATA"/>&#xA0;<xsl:value-of select="FIELD[@name='weight_for_TV']/@unit"/><br/>
                                    </td>
                                </tr>
                            </xsl:when>
                        </xsl:choose>
                    </table>
                    <div style="margin-right:10px;"><xsl:value-of select="FIELD[@name='review']/DATA" disable-output-escaping="yes"/></div>
                    <table width="100%" style="margin:15px 0px; border-spacing: 0px;">
                        <tr>
                            <td style="padding-left:10px;" width="50%">
                                <xsl:if test="FIELD[@name='price']/FIELD[@name='price1'] != '99999999'">
                                <div class="btn-cart cartadd" id="{../@node}">
                                    <input type="hidden" class="cart_product" name="cart_product" value="{../@node}"/>
                                    <div style="text-decoration:underline; background:url('../i/korzina_small.jpg') no-repeat; background-position: left; padding-left:30px;">Добавить в корзину</div>
                                </div> 
                                </xsl:if>
                                <!--div class="btn-cart" onclick="$('.cart-dialog').dialog('open')">
                                    <xsl:if test="FIELD[@name='price']/FIELD[@name='price1'] != ''">
                                        <div style="text-decoration:underline; background:url('../i/korzina_small.jpg') no-repeat; background-position: left; padding-left:30px;">Добавить в корзину</div>
                                        <div class="cart-dialog">
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        Добавить в корзину выбранный товар:<br/>
                                                        <xsl:value-of select="FIELD[@name='title']/DATA"/>?
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" align="right">
                                                        Количество:
                                                    </td>
                                                    <td width="50%">
                                                        <input type="hidden" class="cart_product" name="cart_product" value="{../@node}"/>
                                                        <input type="text" class="cart_count" name="cart_count" value="1" style="width: 35px"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <div class="cartadd">Добавить</div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </xsl:if>
                                </div-->
                            </td>
                            <td>
                                <xsl:if test="FIELD[@name='cost']/DATA!=''">
                                    <div class="old_cost"><xsl:value-of select="format-number(FIELD[@name='cost']/DATA, '### ##0', 'my')"/>&#xA0;<xsl:value-of select="FIELD[@name='cost']/@unit"/></div>
                                </xsl:if>
                                <div class="catalog-cost">
                                    <xsl:choose>
                                        <xsl:when test="FIELD[@name='price']/FIELD[@name='price1'] != '99999999'">
                                            <div style="text-align:center;"><xsl:value-of select="format-number(FIELD[@name='price']/FIELD[@name='price1'], '### ##0', 'my')"/>&#xA0;<xsl:value-of select="FIELD[@name='cost']/@unit"/></div>
                                            <!--div style="text-align:center;"><xsl:value-of select="format-number(FIELD[@name='price']/FIELD[@name='price1'], '### ##0.00', 'my')"/>&#xA0;<xsl:value-of select="FIELD[@name='cost']/@unit"/></div-->
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <div style="text-align:center;">Цену уточняйте</div>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding:5px;">
                    <hr/>
                    <table width="100%" style="margin-top:10px; font-size:14px; border-spacing: 4px;">
                        <xsl:for-each select="FIELD[@spec='yes' and DATA != '']">
                            <tr>
                                <xsl:if test="position() mod 2 = 1">
                                    <xsl:attribute name="style">background-color: #dbdad9;</xsl:attribute>
                                </xsl:if>
                                <td width="40%" valign="top" align="right" style="padding:3px 0px; padding-right: 8px; font-weight:bold;">
                                    <xsl:value-of select="@caption"/>:
                                </td>
                                <td valign="top" style="padding-left: 7px;">
                                    <xsl:choose>
                                        <xsl:when test="@type='list'">
                                            <xsl:for-each select="DATA">
                                                <xsl:variable name="list" select="."/>
                                                <xsl:value-of select="//LIST[@id=$list]/@value"/><xsl:if test="position() != last()">,</xsl:if>&#xA0;
                                            </xsl:for-each>
                                            <xsl:value-of select="@unit"/>    
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="DATA"/>&#xA0;<xsl:value-of select="@unit"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                </td>
            </tr>
        </table>
    </xsl:template>
</xsl:stylesheet>