<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="NODE[@class='file']">
        <div class="file">
            <xsl:variable name="currealm" select="@realm"/>
            <div style="display: inline-block; margin-right: 6px"><a href="/{//SECTION[@realm=$currealm]/@name}?node={@node}" class="link_articles"><xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/>&#xA0;скачать</a></div>
            <a href="/{//SECTION[@realm=$currealm]/@name}?node={@node}"><xsl:value-of select="DATATYPE/FIELD[@name='descr']/BRIEF"/></a>
            <div class="viewcounter">(Количество просмотров: <xsl:value-of select="@viewcounter"/>)</div>
        </div>
    </xsl:template>
    <xsl:template match="DATATYPE[@name='file']">
        <div class="block">
            <div class="title">
                <xsl:value-of select="FIELD[@name='title']/DATA"/>
            </div>
            <div class="content">
                <xsl:value-of select="FIELD[@name='descr']/DATA" disable-output-escaping="yes"/>
            </div>
            <div align="left" style="padding: 10px 5px 20px 0px">
                <xsl:variable name="realm" select="//REALM/@id"/>
                <a href="/public/files/node/{../@node}/{FIELD[@name='file1']/DATA}" class="link" style="font-size: 14px">Скачать файл</a>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>