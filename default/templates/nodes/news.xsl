<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="NODE[@class='news']">
        <div class="news-container">
            <xsl:variable name="realm" select="@realm"/>
            <div style="margin-bottom: 5px">
                <div class="title">
                    <a href="/{//SECTION[@realm=$realm]/@name}?node={@node}"><xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/></a>
                </div>
                <div class="date">
                    <xsl:value-of select="DATATYPE/FIELD[@name='pubdate']/DATA"/>
                </div>
                <div style="clear: both"/>
            </div>
            <div class="content" align="left">
                <div style="float: left; margin-right: 15px">
                    <a href="/{//SECTION[@realm=$realm]/@name}?node={@node}">
                        <xsl:choose>
                            <xsl:when test="DATATYPE/FIELD[@name='image1']/DATA != ''">
                                <img src="/i/x.gif" width="90px" height="60px" style="background-image: url({DATATYPE/FIELD[@name='image1']/IMAGE[@prefix='prev']}); background-size: contain"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <img src="/img/no-image.png" width="80px"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </a>
                </div>  
                <xsl:value-of select="DATATYPE/FIELD[@name='content']/BRIEF" disable-output-escaping="yes"/>...
                <a href="/{//SECTION[@realm=$realm]/@name}?node={@node}" class="link">подробнее &gt;&gt;&gt;</a>  
            </div>
        </div>
    </xsl:template>
    <xsl:template match="DATATYPE[@name='news']">
        <div class="block">
            <div class="title">
                <div class="news-header-text">
                    <xsl:value-of select="FIELD[@name='title']/DATA"/>
                </div>
                <div class="news-date" align="right" valign="top">
                    <xsl:value-of select="FIELD[@name='pubdate']/DATA"/>
                </div>
                <div style="clear: both"/>
            </div>
            <div class="news-content">
                <img src="{FIELD[@name='image1']/IMAGE[@prefix='full']}" class="news-image prev" style="float: left; margin-right: 10px; width: 240px"/>
                <xsl:value-of select="FIELD[@name='content']/DATA" disable-output-escaping="yes"/>
            </div>
            <div align="right" style="padding: 10px 20px 20px 0px">
                <xsl:variable name="realm" select="//REALM/@id"/>
                <a href="/{//SECTION[@realm=$realm]/@name}" class="link">Назад</a>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>