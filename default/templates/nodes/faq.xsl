<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="DATATYPE[@name='faq']">
        <div>
            <xsl:if test="position() mod 2 = 1">
                <xsl:attribute name="style">
                      
                </xsl:attribute>
            </xsl:if>
            <div class="faq">
                <table width="100%" border="0px">
                    <!--tr>
                        <td colspan="2" class="faq-header">
                            <xsl:value-of select="FIELD[@name='qtitle']/DATA" disable-output-escaping="yes"/>
                        </td>
                    </tr-->
                    <tr>
                        <td class="faq-header">
                            Автор:&#xA0;<xsl:value-of select="FIELD[@name='quser']/DATA"/>
                        </td>
                        <td align="right" class="faq-date">
                            <xsl:value-of select="FIELD[@name='qtime']/DATA"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="faq-content">
                            <xsl:value-of select="FIELD[@name='question']/DATA" disable-output-escaping="yes"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="faq-content">
                            <a class="action_link" id="{../@node}">Показать ответ</a>
                        </td>
                    </tr>
                </table>
                <div id="answer{../@node}" style="display: none;">
                    <table width="100%" border="0px">
                        <tr>
                            <td class="faq-header">
                                Ответ:
                            </td>
                            <td align="right" class="faq-date">
                                <xsl:value-of select="FIELD[@name='atime']/DATA"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="faq-content">
                                <xsl:value-of select="FIELD[@name='answer']/DATA" disable-output-escaping="yes"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>