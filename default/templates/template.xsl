<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;"><!ENTITY raquo "&#0187;"><!ENTITY laquo "&#0171;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />  
    <xsl:template match="/DOCUMENT">
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>
                <xsl:if test="//PARAM[@name='node']">
                    <xsl:variable name="node" select="//PARAM[@name='node']"/>
                    <xsl:value-of select="//NODE[@node=$node]/@name"/>&#xA0;|&#xA0;
                </xsl:if>
                <xsl:for-each select="//SECTION[contains(//REALM/@name, @name) and @realm != '1000']">
                    <xsl:sort select="position()" data-type="number" order="descending" />
                    <xsl:value-of select="@title"/>&#xA0;|&#xA0;
                </xsl:for-each>
                Костанайская городская больница
            </title>
            <link rel="stylesheet" href="/public/css/common.css" type="text/css" media="screen" charset="utf-8"/>
            <script src="/public/js/jquery.js" type="text/javascript" charset="utf-8"></script>
            <link rel="stylesheet" type="text/css" href="/public/css/shadowbox.css" />
            <script type="text/javascript" src="/public/js/shadowbox.js"></script>
            <script src="/public/js/lib.js" type="text/javascript" charset="utf-8"></script>   
        </head> 
        <body align="center">
            <xsl:variable name="realm" select="//REALM/@id"/>
            <xsl:variable name="root" select="//SECTION[@parent='1000' and contains(//REALM/@name, @name)]/@realm"/>
            <xsl:variable name="lang" select="substring(/DOCUMENT/REALM/@name,1,2)"/>
            <div class="wrapper">
				<div class="siteheader">
					<table cellpadding="0px" cellspacing="0px" border="0px" width="100%">
						<tr>
							<td rowspan="2" width="170px">
								<a href="/"><div style="width: 170px; height: 153px"/></a>
							</td>
							<td>
								<div class="title1">Коммунальное государственное предприятие</div>
								<div class="title2">КОСТАНАЙСКАЯ ГОРОДСКАЯ БОЛЬНИЦА</div>
							</td>
							<td width="160px;">
								<div class="phone1">Справочная служба</div>
								<div class="phone2">53-52-67</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="top-links">
									<a href="/{$lang}"><img src="/public/i/home.jpg"/></a>&#xA0;&#xA0;
									<a href="/{$lang}/contacts"><img src="/public/i/letter.jpg"/></a>&#xA0;&#xA0;
									<a href="/{$lang}/map"><img src="/public/i/map.jpg"/></a>&#xA0;&#xA0;
								</div>
								<!--div class="lang">
									РУС&#xA0;|&#xA0;<a href="/kz">КАЗ</a>
								</div-->
							</td>
							<td style="padding-top: 16px">
								<div class="right-link">
									<a href="/{$lang}/contacts">Телефоны отделений</a>
								</div>
								<div class="right-link">
									<a href="/{$lang}/faq">Оставить Ваш отзыв</a>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="top-menu-wrapper">
					<div class="leftblock"><div class="corner"/></div>
					<table cellpadding="0px" cellspacing="0px" border="0px" width="100%" class="top-menu"><tr>
						<td align="center">
							<xsl:choose>
								<xsl:when test="//REALM/@id = $root">
									<xsl:attribute name="class">item active</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class">item</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<a href="/{$lang}">Главная</a>
						</td>
						<xsl:for-each select="//SECTION[@parent=$root and @weight &gt; 99]">
							<td align="center">
								<xsl:choose>
									<xsl:when test="//REALM/@id = @realm">
										<xsl:attribute name="class">item active</xsl:attribute>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="class">item</xsl:attribute>
									</xsl:otherwise>
								</xsl:choose>
								<a href="/{@name}"><xsl:value-of select="@title"/></a>
							</td>
						</xsl:for-each>
					</tr></table>
					<div class="rightblock"><div class="corner"/></div>
				</div>
				<table cellpadding="0px" cellspacing="0px" border="0px" width="100%">
					<tr>
						<td width="290px" valign="top">
							<div class="left-menu-wrapper">
								<div class="left-menu">
									<xsl:for-each select="//SECTION[@parent='1003']">
										<div class="item">
											<a class="link" href="/{@name}"><xsl:value-of select="@title"/></a>
										</div>
									</xsl:for-each>
								</div>
                                <div class="corner"/>
							</div>
                            <div class="left-pollblock-wrapper">
                                <div class="left-pollblock">
                                    <div class="title"><xsl:value-of select="//POLLS/ROW/FIELD[@name='question']"/></div>
                                    <div class="content">
                                        <xsl:variable name="poll" select="//POLLS/ROW/@pk"/>
                                        <xsl:choose>
                                            <xsl:when test="//PARAMS[@source='cookie']/PARAM[@name='get_poll'] = $poll">
                                                <table>
                                                    <xsl:variable name="total" select="//POLLS/ROW/FIELD[@name='total']"/>
                                                    <tr>
                                                        <td colspan="2" class="poll-answer" style="padding-bottom: 10px">
                                                            (всего проголосовало: <xsl:value-of select="$total"/>)
                                                        </td>
                                                    </tr>
                                                    <xsl:for-each select="//POLLS/ROW/ANSWERS/ROW">
                                                    <tr>
                                                        <td class="poll-answer" >
                                                            <xsl:value-of select="FIELD[@name='title']"/>
                                                        </td>
                                                        <td align="left" class="poll-answer"  style="padding-left: 6px;">
                                                            <xsl:value-of select="FIELD[@name='count']"/>
                                                        </td>
                                                    </tr>
                                                    </xsl:for-each>
                                                </table>
                                                <div style="padding: 10px 4px">
                                                    <a href="/{$lang}/poll" class="poll-link">Результаты</a>
                                                </div>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <form name="poll" method="post">
                                                    <table>
                                                        <xsl:for-each select="//POLLS/ROW/ANSWERS/ROW">
                                                            <tr>
                                                                <td>
                                                                    <input type="radio" name="answer" value="{@pk}"/>
                                                                </td>
                                                                <td class="poll-answer" align="left">
                                                                    <xsl:value-of select="FIELD[@name='title']"/>
                                                                </td>
                                                            </tr>
                                                        </xsl:for-each>
                                                    </table>
                                                </form>
                                                <table style="margin-top: 8px">
                                                    <tr>
                                                        <td style="padding: 4px 6px">     
                                                            <div class="poll-btn" onclick="poll.submit()">Голосовать</div>
                                                        </td>
                                                        <td>
                                                            <a href="/{$lang}/poll" class="poll-link">Результаты</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </div>
                                </div>
                                <div class="corner"/>
                            </div>
                            <div class="left-phones">
                                <div class="title">
                                    <xsl:value-of select="//LATEST[@type='articles']//NODE[NAME='phones']//FIELD[@name='title']/DATA"/>
                                </div>
                                <div class="content">
                                    <xsl:value-of select="//LATEST[@type='articles']//NODE[NAME='phones']//FIELD[@name='content']/DATA" disable-output-escaping="yes"/>
                                </div>
                            </div>
						</td>
                        <td class="mid-grad"/>
						<td valign="top" align="left">
							<xsl:if test="//REALM/@id != $root">
                                <div class="way">
    								<xsl:for-each select="//SECTION[contains(//REALM/@name, @name) and @realm != 1000]">
    									<xsl:choose>
    										<xsl:when test="@realm = $realm">
    											<b><xsl:value-of select="@title"/></b>   
    										</xsl:when>
    										<xsl:otherwise>
    											<a href="/{@name}"><xsl:value-of select="@title"/></a>&#xA0;/
    										</xsl:otherwise>
    									</xsl:choose>
    								</xsl:for-each>
    							</div>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test="//REALM/@name='ru'">
                                    <div class="slider-wrapper">
                                        <div class="left"/>
                                        <div class="slides">
                                            <xsl:for-each select="//LATEST[@type='slider']/NODE">
                                                <div class="item" id="slide{@node}" style="background-image: url({DATATYPE/FIELD[@name='image']/IMAGE[@prefix='full']}); background-repeat: no-repeat; background-size: cover;">
                                                    <div class="cnt">
                                                        <a href="{DATATYPE/FIELD[@name='link']/DATA}">
                                                            <div style="width: 630px; height: 280px"/>
                                                        </a>    
                                                    </div>
                                                    <div class="text-block">
                                                        <xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/>
                                                    </div>
                                                    <div class="nav">
                                                        <xsl:variable name="nd" select="@node"/>
                                                        <xsl:for-each select="//LATEST[@type='slider']/NODE">
                                                            <div data-id="{@node}">
                                                                <xsl:choose>
                                                                    <xsl:when test="@node = $nd">
                                                                        <xsl:attribute name="class">active</xsl:attribute>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:attribute name="class">btn</xsl:attribute>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </div>
                                                        </xsl:for-each>
                                                    </div>
                                                </div>
                                            </xsl:for-each>
                                        </div>
                                        <div class="right"/>
                                    </div>
                                    <div class="main-btns">
                                        <div class="btn1">
                                            <a href="/{$lang}/faq"><img src="/public/i/btn1.jpg"/></a>
                                        </div>
                                        <div class="btn2">
                                            <a href="/{$lang}/contacts"><img src="/public/i/btn2.jpg"/></a>
                                        </div>
                                    </div>
                                    <div style="clear: both"/>
                                    <div class="main-news">
                                        <div class="title">ПОЛЕЗНАЯ ИНФОРМАЦИЯ</div>
                                        <xsl:for-each select="//LATEST[@type='poleznoe']/NODE">
                                            <xsl:variable name="rlm" select="@realm"/>
                                            <div class="main-news-item">
                                                <div class="img">
                                                    <xsl:choose>
                                                        <xsl:when test="DATATYPE/FIELD[@name='image']/DATA != ''">
                                                            <div style="width: 90px; height: 60px; background-image: url({DATATYPE/FIELD[@name='image']/IMAGE[@prefix='prev']}); background-size: contain"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <div style="width: 90px; height: 60px; background-image: url(/public/i/no-image.png); background-size: 70%; background-repeat: no-repeat; background-position: center"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </div>
                                                <div class="title">
                                                    <a href="/{//SECTION[@realm=$rlm]/@name}?node={@node}"><xsl:value-of select="DATATYPE/FIELD[@name='title']/DATA"/></a>
                                                </div>
                                            </div>
                                        </xsl:for-each>
                                        <div style="clear: both"/>
                                    </div>
                                </xsl:when>
                                <xsl:when test="//REALM/@name='ru/map'">
                                    <div class="block">
                                        <div class="title">
                                            <xsl:value-of select="//REALM/TITLE"/>
                                        </div>
                                        <div class="content">
                                            <xsl:call-template name="sitemap"/>
                                        </div>
                                    </div>
                                </xsl:when>
                                <xsl:when test="//REALM/@name='ru/faq'">
                                    <div class="block">
                                        <div class="title">
                                            <xsl:value-of select="//REALM/TITLE"/>
                                        </div>
                                        <div class="content">
                                            <div>
                                                <xsl:value-of select="//CONTENT/NODE[@name='index']/DATATYPE/FIELD[@name='content']/DATA" disable-output-escaping="yes"/>
                                            </div>
                                            <xsl:if test="//INCLUDE[@name='Faq']/MSG">
                                                <div align="center" style="color: red; font-size: 16px;">
                                                    <xsl:value-of select="//INCLUDE[@name='Faq']/MSG"/>
                                                </div>
                                            </xsl:if>
                                            <form name="faq" action="/{$lang}/faq" method="post">
                                                <table cellpadding="0px" cellspacing="15px" border="0px" width="100%">
                                                    <tr>
                                                        <td colspan="2" align="center" class="faq-form-title">
                                                            Чтобы задать свой вопрос - заполните следующую форму:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" valign="top" class="faq-form-caption">
                                                            Ваше Ф.И.О.:
                                                        </td>
                                                        <td width="70%">
                                                            <input type="text" name="faq[quser]" style="width: 320px;"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" valign="top" class="faq-form-caption">
                                                            E-mail:&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <input type="text" name="faq[email]" style="width: 320px;"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" valign="top" class="faq-form-caption">
                                                            Вопрос:&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <textarea name="faq[question]" style="width: 320px; height: 160px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        
                                                        </td>
                                                        <td>
                                                           
                                                        <img src="/captcha.php"/>
                                                        <input type="hidden" name="cchash" value="{//SESSION/@ch}" style="vertical-align:top"/>
                                                        <input type="text" name="cc" style="width: 60px;vertical-align:top;margin-left:5px;margin-top:0;"/>   
                                                        <input type="submit" value="Задать вопрос" style="vertical-align:top;margin-left:85px;margin-top:0;width:122px;"/> 
                                                        <br/>
                                                        введите код с картинки        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>
                                            <xsl:apply-templates select="/DOCUMENT/CONTENT/NODE[@class='faq']/DATATYPE"/>
                                            <div align="center">
                                                <xsl:apply-templates select="//PAGING"/>
                                            </div>
                                        </div>    
                                    </div>
                                </xsl:when>
                                <xsl:when test="//REALM/@name = 'ru/poll'">
                                    <div class="block">
                                        <div class="title">
                                            Текущий опрос
                                        </div>
                                        <div class="content" align="left" style="padding-top: 5px">
                                            <div>   
                                                <xsl:variable name="poll" select="//POLLS/ROW/@pk"/>
                                                <xsl:choose>
                                                    <xsl:when test="//PARAMS[@source='cookie']/PARAM[@name='get_poll'] = $poll">
                                                        <table width="100%">
                                                            <xsl:variable name="total" select="//POLLS/ROW/FIELD[@name='total']"/>
                                                            <tr>
                                                                <td colspan="2" class="poll-title" align="left">
                                                                    <xsl:value-of select="//POLLS/ROW/FIELD[@name='question']"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" class="poll-answer" style="padding-bottom: 10px">
                                                                    (всего проголосовало: <xsl:value-of select="$total"/>)
                                                                </td>
                                                            </tr>
                                                            <xsl:for-each select="//POLLS/ROW/ANSWERS/ROW">
                                                            <!--tr>
                                                                <td colspan="2">
                                                                    <div style="height: 20px; background-color: #1377BF; width: {@percent}%;"/>
                                                                </td>
                                                            </tr-->
                                                            <tr>
                                                                <td class="poll-answer" >
                                                                    <xsl:value-of select="FIELD[@name='title']"/>
                                                                </td>
                                                                <td align="left" class="poll-answer"  style="padding-left: 6px;">
                                                                    <xsl:value-of select="FIELD[@name='count']"/>
                                                                </td>
                                                            </tr>
                                                            </xsl:for-each>
                                                        </table>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <div class="poll-title">
                                                            <xsl:value-of select="//POLLS/ROW/FIELD[@name='question']"/>
                                                        </div>
                                                        <form name="poll1" method="post">
                                                            <table>
                                                                <xsl:for-each select="//POLLS/ROW/ANSWERS/ROW">
                                                                    <tr>
                                                                        <td>
                                                                            <input type="radio" name="answer" value="{@pk}"/>
                                                                        </td>
                                                                        <td class="poll-answer">
                                                                            <xsl:value-of select="FIELD[@name='title']"/>
                                                                        </td>
                                                                    </tr>
                                                                </xsl:for-each>
                                                            </table>
                                                        </form>
                                                        <table style="margin-top: 8px">
                                                            <tr>
                                                                <td style="padding: 4px 6px">     
                                                                    <div class="poll-btn" onclick="poll1.submit()">Голосовать</div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block">
                                        <div class="title">
                                            Результаты
                                        </div>
                                        <div class="content" align="left" style="padding-top: 5px">
                                            <div>   
                                                <xsl:variable name="poll" select="//POLLS/ROW/@pk"/>
                                                <xsl:for-each select="//ALLPOLLS/ROW">
                                                    <table width="100%">
                                                        <xsl:variable name="total" select="FIELD[@name='total']"/>
                                                        <tr>
                                                            <td colspan="2" class="poll-title" align="left">
                                                                <xsl:value-of select="FIELD[@name='question']"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="poll-answer" style="padding-bottom: 10px">
                                                                (всего проголосовало: <xsl:value-of select="$total"/>)
                                                            </td>
                                                        </tr>
                                                        <xsl:for-each select="ANSWERS/ROW">
                                                        <!--tr>
                                                            <td colspan="2">
                                                                <div style="height: 20px; background-color: #1377BF; width: {@percent}%;"/>
                                                            </td>
                                                        </tr-->
                                                        <tr>
                                                            <td class="poll-answer" >
                                                                <xsl:value-of select="FIELD[@name='title']"/>
                                                            </td>
                                                            <td align="left" class="poll-answer"  style="padding-left: 6px;">
                                                                <xsl:value-of select="FIELD[@name='count']"/>
                                                            </td>
                                                        </tr>
                                                        </xsl:for-each>
                                                    </table><BR/>
                                                </xsl:for-each>
                                            </div>
                                        </div>
                                    </div>
                                </xsl:when>
                                <xsl:when test="//REALM/@id = '1009'">
                                    <div class="block">
        								<div class="title">
                                            <xsl:value-of select="//REALM/TITLE"/>
        								</div>
        								<div class="content">
                                            <xsl:apply-templates select="/DOCUMENT/CONTENT/NODE/DATATYPE"/>
        								</div>
                                        <div class="content">
                                            <xsl:text disable-output-escaping="yes">
                                                <![CDATA[
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4777.145914427762!2d63.612512068754235!3d53.225504869248425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43cc8a7c117a3be3%3A0xa4d1d077957b3ab9!2z0JPQvtGA0L7QtNGB0LrQsNGPINCx0L7Qu9GM0L3QuNGG0LA!5e0!3m2!1sru!2sru!4v1408642116178" width="600" height="450" frameborder="0" style="border:0"></iframe>
                                                ]]>
                                            </xsl:text>
                                        </div>
        							</div>
                                </xsl:when>
                                <xsl:when test="count(/DOCUMENT/CONTENT/NODE) = 1">
                                    <div class="block">
        								<div class="title">
                                            <xsl:value-of select="//REALM/TITLE"/>
        								</div>
        								<div class="content">
                                            <xsl:apply-templates select="/DOCUMENT/CONTENT/NODE/DATATYPE"/>
        								</div>
        							</div>
                                </xsl:when>
                                <xsl:when test="//PARAM[@name='node']">
                                    <xsl:apply-templates select="/DOCUMENT/NODE/DATATYPE"/> 
                                </xsl:when>
                                <xsl:otherwise>
                                    <div class="block">
        								<div class="title">
        									<xsl:choose>
        										<xsl:when test="//PARAM[@name='node']">
        											<xsl:value-of select="/DOCUMENT/NODE/DATATYPE/FIELD[@name='title']/DATA"/>
        										</xsl:when>
        										<xsl:otherwise>
        											<xsl:value-of select="//REALM/TITLE"/>
        										</xsl:otherwise>
        									</xsl:choose>
        								</div>
        								<div class="content">
        									<xsl:choose>
        										<xsl:when test="//PARAM[@name='node']">
        											<xsl:apply-templates select="/DOCUMENT/NODE/DATATYPE"/>
        										</xsl:when>
        										<xsl:otherwise>
        											<xsl:apply-templates select="/DOCUMENT/CONTENT/NODE"/> 
        										</xsl:otherwise>
        									</xsl:choose>    
        								</div>
        							</div>
                                </xsl:otherwise>
                            </xsl:choose>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table cellpadding="0px" cellspacing="0px" width="100%" border="0px">
						<tr>    
							<td align="left">
                                <div>КГП «Костанайская городская больница»</div>
                                <div>
                                    <a href="/{$lang}/contacts">Контактная информация</a>&#xA0;&#xA0;|&#xA0;&#xA0;<a href="/{$lang}/contacts#map">Схема проезда</a>
                                </div>
                            </td>
							<td></td>
							<td align="right">
                                <a href="http://axon.kz">Разработка сайта</a>
                                <div>Комания «Аксон»</div>
                            </td>
						</tr>
					</table>
				</div>

			</div>
		</body>
    </html>
    </xsl:template>
    <xsl:include href="public/templates/templates.xsl"/>
</xsl:stylesheet>