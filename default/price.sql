CREATE TABLE IF NOT EXISTS `T_PRICE` (
  `node` int(12) NOT NULL,
  `field` int(5) NOT NULL,
  `extkey` varchar(18) NOT NULL,
  `count` int(12) NOT NULL,
  `price1` varchar(18) NOT NULL DEFAULT '0',
  `price2` varchar(18) NOT NULL,
  `price3` varchar(18) NOT NULL,
  UNIQUE KEY `node-field` (`node`,`field`),
  KEY `node` (`node`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `T_REALM` (`realm`, `name`, `title`, `parent`, `auth`, `weight`, `realmpagesize`, `template`, `image`, `descr`, `nodecount`) VALUES
(75, 'panel/module/price', 'Прайс', 13, 3, 100, 100, 'panel.xsl', '', '', 0),
(76, 'panel/module/price/loadprice', 'Загрузить прайс', 75, 3, 100, 100, 'panel.xsl', '', '', 0);
INSERT INTO `T_CONF` (`namespace`, `confkey`, `confvalue`) VALUES ('price', 'pagesize', '100');
INSERT INTO `T_CONF` (`namespace`, `confkey`, `confvalue`) VALUES ('price', 'dt', '\'catalog\'');
INSERT INTO `T_INCLUDE` (`include`, `module`, `class`, `title`, `xsl`, `dash`) VALUES (20, 'price.inc.php', 'Price', 'Прайс', '', 'on');
