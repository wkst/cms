CREATE TABLE IF NOT EXISTS `T_ORDER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `phone` text NOT NULL,
  `status` int(11) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `node` int(11) NOT NULL,
  `date` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `T_REALM` (`realm`, `name`, `title`, `parent`, `auth`, `weight`, `realmpagesize`, `template`, `image`, `descr`, `nodecount`) VALUES
(40, 'panel/module/cart', 'Интернет магазин', 2, 3, 100, 100, 'panel.xsl', '', '', 0);
(41, 'panel/module/cart/order', 'Просмотр заказа', 2, 3, 100, 100, 'panel.xsl', '', '', 0);
INSERT INTO `T_INCLUDE` (`include`, `module`, `class`, `title`, `xsl`) VALUES (9, 'cart.inc.php', 'Cart', 'Корзина заказов', '');

