CREATE TABLE IF NOT EXISTS `T_ANSWER` (
  `answer` int(11) NOT NULL AUTO_INCREMENT,
  `poll` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`answer`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
INSERT INTO `T_ANSWER` (`answer`, `poll`, `title`, `count`) VALUES
	(32, 13, 'Да', 95),
	(33, 13, 'Не совсем', 36),
	(34, 13, 'Нет', 47),
	(35, 14, 'Плохо', 418),
	(36, 14, 'Удовлетворительно', 179),
	(37, 14, 'Хорошо', 1115),
	(38, 16, 'Вариант ответа 1', 0),
	(39, 16, 'Вариант ответа 2', 1),
	(40, 16, 'Вариант ответа 3', 1),
	(55, 19, 'да', 0),
	(56, 19, 'нет', 0),
	(57, 19, 'незнаю', 1);

CREATE TABLE IF NOT EXISTS `T_COMENT` (
  `coment` int(11) NOT NULL AUTO_INCREMENT,
  `node` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `date` int(12) NOT NULL,
  `text` text NOT NULL,
  `moderated` int(1) NOT NULL,
  UNIQUE KEY `coment` (`coment`),
  KEY `node` (`node`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_CONF` (
  `namespace` varchar(32) DEFAULT NULL,
  `confkey` varchar(32) NOT NULL,
  `confvalue` varchar(5000) NOT NULL,
  UNIQUE KEY `key` (`confkey`,`namespace`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `T_CONF` (`namespace`, `confkey`, `confvalue`) VALUES
	('global', 'email', 'wkst@mail.ru'),
	('global', 'cache', 'file'),
	('global', 'url', 'http://cms.smarthost.kz'),
	('global', 'name', 'cms.smarthost.kz'),
	('global', 'libdir', 'core'),
	('global', 'moddir', 'modules'),
	('global', 'xsldir', 'public/templates'),
	('global', 'filesdir', 'public/files'),
	('global', 'cachedir', 'public/cache'),
	('global', 'default_template', 'template.xsl'),
	('global', 'root', '1000'),
	('global', 'defaultpagesize', '10'),
	('global', 'sault', 'qewrtg463rszssdfs'),
	('global', 'default_uri', '/'),
  ('global', 'rulersize', '20'),
  ('global', 'brief', '100');

CREATE TABLE IF NOT EXISTS `T_GROUP` (
  `gid` int(12) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(32) DEFAULT NULL,
  `url` varchar(32) NOT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `T_GROUP` (`gid`, `groupname`, `url`) VALUES
	(3, 'Administrators', '/panel'),
	(0, 'nobody', '/'),
	(1, 'Registered', '/cart');

CREATE TABLE IF NOT EXISTS `T_INCLUDE` (
  `include` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(32) DEFAULT NULL,
  `class` varchar(255) CHARACTER SET ucs2 NOT NULL,
  `title` varchar(255) NOT NULL,
  `xsl` varchar(32) DEFAULT NULL,
  `dash` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`include`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 PACK_KEYS=0;

INSERT INTO `T_INCLUDE` (`include`, `module`, `class`, `title`, `xsl`) VALUES
	(1, 'callback.inc.php', 'Callback', 'Обратная связь', NULL),
	(2, 'user.inc.php', 'User', 'Пользовательская информация', NULL),
	(3, 'faq.inc.php', 'Faq', 'Вопрос-ответ', ''),
	(4, 'coment.inc.php', 'Coment', 'Коментарии', ''),
	(5, 'latest.inc.php', 'Latest', 'Последняя информация', ''),
	(6, 'adds.inc.php', 'Adds', 'Материалы из подразделов', ''),
	(7, 'lists.inc.php', 'Lists', 'Списки', ''),
	(8, 'search.inc.php', 'Search', 'Поиск', ''),
(10, 'poll.inc.php', 'Poll', 'Голосования', '');

CREATE TABLE IF NOT EXISTS `T_INCLUDE_REALM` (
  `realm` int(11) NOT NULL,
  `include` int(11) NOT NULL,
  `mode` int(1) NOT NULL,
  KEY `realm` (`realm`,`include`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `T_INCLUDE_REALM` (`realm`, `include`, `mode`) VALUES
	(1000, 2, 1);

CREATE TABLE IF NOT EXISTS `T_LIST` (
  `list` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`list`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO `T_LIST` (`list`, `parent`, `value`, `url`) VALUES
	(1, 0, 'root', 'root'),
	(2, 1, 'Bool', 'root/bool'),
	(3, 2, 'да', 'root/bool/yes'),
	(4, 2, 'нет', 'root/bool/no');

CREATE TABLE IF NOT EXISTS `T_LOGIN` (
  `login` varchar(32) DEFAULT NULL,
  `hash` varchar(64) DEFAULT NULL,
  `su` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `T_LOGIN` (`login`, `hash`, `su`) VALUES
	('admin', '698d51a19d8a121ce581499d7b701668', 1),
	('nobody', NULL, 0);

CREATE TABLE IF NOT EXISTS `T_LOGIN_GROUP` (
  `login` varchar(255) NOT NULL,
  `gid` int(11) NOT NULL,
  UNIQUE KEY `login` (`login`,`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `T_LOGIN_GROUP` (`login`, `gid`) VALUES
	('admin', 3);

CREATE TABLE IF NOT EXISTS `T_NODE` (
  `node` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `class` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `disabled` int(1) NOT NULL,
  `highlight` int(12) NOT NULL,
  `topcounter` int(12) NOT NULL,
  `viewcounter` int(12) NOT NULL,
  `userrating` int(12) NOT NULL,
  `time` int(12) NOT NULL,
  PRIMARY KEY (`node`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_NODE_DATA` (
  `node` int(11) NOT NULL,
  `dataid` int(11) NOT NULL,
  `value` text NOT NULL,
  `dataindex` int(10) NOT NULL,
  KEY `node_2` (`node`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_NODE_DATANAME` (
  `dataid` int(11) NOT NULL AUTO_INCREMENT,
  `dataname` varchar(255) NOT NULL,
  PRIMARY KEY (`dataid`),
  UNIQUE KEY `dataname` (`dataname`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_NODE_RATING` (
  `login` varchar(255) NOT NULL,
  `node` int(11) NOT NULL,
  `rating` int(1) NOT NULL,
  `session_hash` varchar(16) NOT NULL,
  UNIQUE KEY `key` (`session_hash`,`node`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_NODE_REALM` (
  `node` int(11) NOT NULL,
  `realm` int(11) NOT NULL,
  `mode` int(1) NOT NULL,
  KEY `node` (`node`),
  KEY `realm` (`realm`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `T_POLL` (
  `poll` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL,
  `lang` varchar(12) NOT NULL,
  PRIMARY KEY (`poll`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

INSERT INTO `T_POLL` (`poll`, `question`, `active`, `total`, `lang`) VALUES
	(13, 'Довольны ли вы качеством обслуживания?', 0, 178, 'ru'),
	(14, 'Ваша оценка оказанной услуги Предварительная запись на прием', 0, 1712, 'ru'),
	(16, 'Опрос на казахском', 1, 2, 'kz');

CREATE TABLE `T_REALM` (
  `realm` INT(12) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `parent` INT(12) NOT NULL,
  `auth` INT(1) NOT NULL,
  `weight` INT(11) NOT NULL,
  `realmpagesize` INT(5) NOT NULL,
  `template` VARCHAR(255) NOT NULL,
  `image` VARCHAR(255) NOT NULL,
  `descr` TEXT NOT NULL,
  `nodecount` INT(11) NOT NULL,
  `metadescr` VARCHAR(500) NULL,
  `metakey` VARCHAR(500),
  PRIMARY KEY (`realm`)
)
  COLLATE='utf8_general_ci'
  ENGINE=MyISAM
  AUTO_INCREMENT=1001;
INSERT INTO `T_REALM` (`realm`, `name`, `title`, `parent`, `auth`, `weight`, `realmpagesize`, `template`, `image`, `descr`, `nodecount`) VALUES
	(1, 'login', 'Вход', 0, 0, 0, 0, 'login.xsl', '', '', 0),
	(2, 'panel', 'Панель управления', 0, 3, 0, 0, 'panel.xsl', '', '', 0),
	(3, 'panel/clear_cache', 'Очистить файловый кеш', 2, 3, 0, 0, 'panel.xsl', '', '', 0),
	(6, 'panel/addrealm', 'Добавить раздел', 2, 3, 100, 0, 'realm.xsl', '', '', 0),
	(7, 'panel/editrealm', 'Редактирование раздела', 2, 3, 100, 0, 'realm.xsl', '', '', 0),
	(8, 'panel/deleterealm', 'Удалить раздел', 2, 3, 100, 0, 'panel.xsl', '', '', 0),
	(10, 'panel/addnode', 'Добавить элемент', 2, 3, 100, 0, 'node.xsl', '', '', 0),
	(11, 'panel/editnode', 'Редактировать элемент', 2, 3, 100, 0, 'node.xsl', '', '', 0),
	(12, 'panel/deletenode', 'Удалить элемент', 2, 3, 100, 0, 'panel.xsl', '', '', 0),
  (13, 'panel/module', 'Настройка модулей', 2, 3, 100, 0, 'panel.xsl', '', '', 0),
	(15, 'panel/users', 'Пользователи', 2, 3, 100, 0, 'panel.xsl', '', '', 0),
	(16, 'panel/adduser', 'Добавить пользователя', 2, 3, 100, 0, 'user.xsl', '', '', 0),
	(17, 'panel/groups', 'Группы доступа', 2, 3, 200, 200, 'panel.xsl', '', '', 0),
	(18, 'panel/editrealmgroup', 'Группы раздела', 2, 3, 200, 200, 'panel.xsl', '', '', 0),
	(19, 'panel/editusergroup', 'Группы пользователя', 2, 3, 200, 200, 'group.xsl', '', '', 0),
	(20, 'panel/edituser', 'Редактирование пользователя', 2, 3, 200, 200, 'user.xsl', '', '', 0),
	(21, 'panel/include', 'Модули', 2, 3, 100, 100, 'panel.xsl', '', '', 0),
	(22, 'panel/insertinclude', 'Добавить модуль', 2, 3, 100, 100, 'module.xsl', '', '', 0),
	(23, 'panel/editinclude', 'Редактировать модуль', 2, 3, 100, 100, 'module.xsl', '', '', 0),
	(24, 'panel/deleteinclude', 'Удалить модуль', 2, 3, 100, 100, 'panel.xsl', '', '', 0),
	(25, 'panel/lists', 'Списки', 2, 3, 120, 100, 'panel.xsl', '', '', 0),
	(26, 'panel/addlist', 'Добавить список', 2, 3, 100, 10, 'list.xsl', '', '', 0),
	(27, 'panel/deletelist', 'Удалить список', 2, 3, 100, 10, 'panel.xsl', '', '', 0),
	(29, 'panel/constructor', 'Конструктор данных', 2, 3, 100, 100, 'panel.xsl', '', '', 0),
	(32, 'panel/config', 'Конфигурация', 2, 3, 100, 1000, 'panel.xsl', '', '', 0),
  (33, 'panel/addgroup', 'Добавить группу', 2, 3, 100, 10, 'group.xsl', '', '', 0),
  (34, 'panel/editgroup', 'Редактировать группу', 2, 3, 100, 10, 'group.xsl', '', '', 0),
  (35, 'panel/deleteuser', 'Удалить пользователя', 2, 3, 100, 100, 'panel.xsl', '', '', 0),
  (36, 'panel/deletegroup', 'Удалить группу', 2, 3, 100, 100, 'panel.xsl', '', '', 0),
  (1000, '/', 'Главная', 0, 0, 100, 10, 'template.xsl', '', '', 1);

CREATE TABLE IF NOT EXISTS `T_REALM_GROUP` (
  `realm` int(11) NOT NULL,
  `gid` int(11) NOT NULL,
  `mode` varchar(255) NOT NULL,
  UNIQUE KEY `realm` (`realm`,`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `T_REALM_GROUP` (`realm`, `gid`, `mode`) VALUES
	(2, 3, ''),
	(10, 5, ''),
	(3, 3, ''),
	(6, 3, ''),
	(7, 3, ''),
	(8, 3, ''),
	(9, 3, ''),
	(10, 3, ''),
	(11, 3, ''),
	(12, 3, ''),
	(13, 3, ''),
	(14, 3, ''),
	(15, 3, ''),
	(16, 3, ''),
	(17, 3, ''),
	(18, 3, ''),
	(19, 3, ''),
	(20, 3, ''),
	(21, 3, ''),
	(22, 3, ''),
	(23, 3, ''),
	(24, 3, ''),
	(25, 3, ''),
	(26, 3, ''),
	(27, 3, ''),
	(28, 3, ''),
	(29, 3, ''),
	(30, 3, ''),
	(31, 3, ''),
	(32, 3, ''),
	(33, 3, ''),
	(34, 3, ''),
	(35, 3, ''),
	(36, 3, ''),
	(37, 3, ''),
	(38, 3, ''),
	(39, 3, ''),
	(40, 3, ''),
	(2, 1, '');
CREATE TABLE IF NOT EXISTS `T_SEARCH` (
  `node` int(11) NOT NULL,
  `textdata` text NOT NULL,
  FULLTEXT KEY `textdata` (`textdata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `T_SESSION` (
  `login` varchar(32) DEFAULT NULL,
  `session_hash` varchar(16) NOT NULL DEFAULT '',
  `time` int(11) DEFAULT NULL,
  `params` longtext,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`session_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `T_USERS` (
  `user` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
INSERT INTO `T_USERS` (`user`, `login`, `email`, `name`, `params`, `company`, `phone`) VALUES
	(1, 'admin', 'support@axon.kz', 'admin', '', '', ''),
	(2, 'tester', 'tester@tester.kz', 'tester', '', '', '');