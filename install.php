<?
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
    switch ($_GET['step'])
    {
        case 1:
            if(!$_POST)
            {
                echo "Скрипт установки шаг 1<br />";
                echo "
                <form action=\"?step=1\" method=\"post\">
                    <table>
                        <tr>
                            <td>Имя базы данных</td>
                            <td><input type=\"text\" name=\"dbname\"/></td>
                        </tr>
                        <tr>
                            <td>Пользователь</td>
                            <td><input type=\"text\" name=\"dbuser\"/></td>
                        </tr>
                        <tr>
                            <td>Пароль</td>
                            <td><input type=\"text\" name=\"dbpass\"/></td>
                        </tr>
                    </table>
                    <input type=\"submit\" value=\"Отправить\"/>
                </form>";
            }
            else
            {
                if(!file_exists('dbcfg.php'))
                {
                    echo "Скрипт установки шаг 1<br />";
                    echo "Записываем данные в файл<br />";
                    $cfg = fopen('dbcfg.php', 'w');
                    fwrite($cfg, "<?php\r");
                    fwrite($cfg, "\$cfg[dbhost] = \"localhost\";\r");
                    fwrite($cfg, "\$cfg[dbdriver] = \"mysql\";\r"); 
                    fwrite($cfg, "\$cfg[dbname] = \"".$_POST['dbname']."\";\r");
                    fwrite($cfg, "\$cfg[dbuser] = \"".$_POST['dbuser']."\";\r");
                    fwrite($cfg, "\$cfg[dbpass] = \"".$_POST['dbpass']."\";\r");
                    fwrite($cfg, "php?>");
                    fclose($cfg);
                    echo "Конфигурация сохранена!<br />";
                    echo "<a href=\"?step=2\">Следующий шаг</a>";
                    
                }
                else
                {
                    echo "Настройки БД уже установлены<br />";
                    echo "<a href=\"?step=2\">Следующий шаг</a>";
                } 
            }
            
        break;
        
        case 2:
            include_once("adodb/adodb.inc.php");
            include_once("dbcfg.php");
            echo "Скрипт установки шаг 2<br />";
            $query = file_get_contents('default/dump.sql');
            
            $dumps = explode(";", $query);
            $Connection = ADONewConnection($cfg[dbdriver]);
			if(!$Connection->PConnect($cfg[dbhost],$cfg[dbuser],$cfg[dbpass],$cfg[dbname]))
			echo $Connection->ErrorMsg();
			$Connection->Execute("set CHARACTER SET utf8");
            foreach($dumps as $k)
            {
                $rs = $Connection->Execute($k);
            }
            //if(!$rs)echo $Connection->ErrorMsg();
            /*else*/ echo "База данных загружена!";
            echo "<a href='/'>Перейти на сайт</a>";
        break;
        default:
            echo "Скрипт установки шаг 0<br />";
            if(!is_dir('public'))
            {
                mkdir('public', 0775);
                echo "создаем директорию public ...<br />";
            }
            else echo "public директория уже существует!<br />";
            
            if(!is_dir('public/cache'))
            {
                mkdir('public/cache', 0775);
                echo "создаем директорию public/cache ...<br />";
            }
            else echo "public/cache директория уже существует!<br />";
            if(!is_dir('public/files'))
            {
                mkdir('public/files', 0775);
                echo "создаем директорию public/files ...<br />";
            }
            else echo "public/files директория уже существует!<br />";
            
            if(!is_dir('public/files/elfinder'))
            {
                mkdir('public/files/elfinder', 0775);
                echo "создаем директорию public/files/elfinder ...<br />";
            }
            else echo "public/files/elfinder директория уже существует!<br />";
            
            if(!is_dir('public/files/node'))
            {
                mkdir('public/files/node', 0775);
                echo "создаем директорию public/files/node ...<br />";
            }
            else echo "public/files/node директория уже существует!<br />";
            
            if(!is_dir('public/files/realm'))
            {
                mkdir('public/files/realm', 0775);
                echo "создаем директорию public/files/realm ...<br />";
            }
            else echo "public/files/node директория уже существует!<br />";
            
            if(!is_dir('public/templates'))
            {
                mkdir('public/templates', 0775);
                echo "создаем директорию public/templates ...<br />";
                dircpy('', 'default/templates', 'public/templates', false);
                echo "копируем шаблоны сайта по-умолчанию...<br />";
                
            }
            else echo "public/templates директория уже существует!<br />";
            
            
            if(!is_dir('public/css'))
            {
                mkdir('public/css', 0775);
                echo "создаем директорию public/css ...<br />";
                dircpy('', 'default/css', 'public/css', false);
            }
            else echo "public/templates/css директория уже существует!<br />";
            
            if(!is_dir('public/js'))
            {
                mkdir('public/js', 0775);
                echo "создаем директорию public/js ...<br />";
                dircpy('', 'default/js', 'public/js', false);
            }
            else echo "public/templates/js директория уже существует!<br />";
            
            
            echo "<a href=\"?step=1\">Следующий шаг</a>";
        break;
    }
    
    
    function dircpy($basePath, $source, $dest, $overwrite = false)
    {
        
        if(!is_dir($basePath . $dest))mkdir($basePath . $dest);
        if($handle = opendir($basePath . $source))
        {  
            while(false !== ($file = readdir($handle)))
            {
                if($file != '.' && $file != '..')
                {
                    $path = $source . '/' . $file;
                    if(is_file($basePath . $path))
                    { 
                        if(!is_file($basePath . $dest . '/' . $file) || $overwrite)
                        if(!@copy($basePath . $path, $basePath . $dest . '/' . $file))
                        {
                            echo '<font color="red">File (' . $path . ') could not be copied, likely a permissions problem.</font>';
                        }
                    }
                    elseif(is_dir($basePath . $path))
                    {   
                        if(!is_dir($basePath . $dest . '/' . $file))mkdir($basePath . $dest . '/' . $file);
                        dircpy($basePath, $path, $dest . '/' . $file, $overwrite);
                    }
                }
            }
            closedir($handle);
        }
    }
?>