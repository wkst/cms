<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="/DOCUMENT/MODULE/PANELXML">
        <xsl:text disable-output-escaping="yes"><![CDATA[
            <script type="text/javascript">
            $(document).ready(function(){
                $(".price-string").click(function(){
                    id = $(this).attr("data-id");
                    $(".price-edit").hide();
                    $(".price-string").show();
                    $(this).hide();
                    $("#"+id).show();
                });

                $(".save-form").click(function(){
                    id = $(this).attr("data-id");
                    data = Array();
                    $(".price-edit[id="+id+"] input").each(function(){
                        data[this.name] = this.value;
                    });
                    data['output'] = 'ajax';
                    $.post(
                        '/action/saveprice',
                        {
                            output: 'ajax',
                            module: 'price',
                            node: data['node'],
                            extkey: data['extkey'],
                            count: data['count'],
                            price1: data['price1']/*,
                            price2: data['price2'],
                            price3: data['price3']  */
                        },
                        function(response){
                            //alert(response);
                            $(".price-string[data-id="+id+"] .extkey").html(data['extkey']);
                            $(".price-string[data-id="+id+"] .count").html(data['count']);
                            $(".price-string[data-id="+id+"] .price1").html(data['price1']);
                            /*$(".price-string[data-id="+id+"] .price2").html(data['price2']);
                            $(".price-string[data-id="+id+"] .price3").html(data['price3']); */
                            $(".price-edit").hide();
                            $(".price-string[data-id='"+id+"']").show();
                            //location.reload();
                        }
                    );
                });
            });
            </script>]]>
        </xsl:text>
        <xsl:choose>
            <xsl:when test="//REALM/@name='panel/module/price/loadprice'">
                <form name="loadprice" class="nodeform" action="/panel/loadprice" method="post" enctype="multipart/form-data">
                    <input type="file" name="price"/>
                    <input type="submit"/>
                </form>
            </xsl:when>
            <xsl:otherwise>
                <h4>Прайс-лист</h4>
                <a href="/panel/module/price/loadprice">Загрузить прайс</a>
                <table cellpadding="0px" cellspacing="0px" class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th width="20px">#</th>
                            <th width="260px">Заголовок</th>
                            <th>Внешний ключ</th>
                            <th>Раздел</th>
                            <th width="80px">Кол-во</th>
                            <th width="80px">Цена1</th>
                            <th width="40px"></th>
                        </tr>
                    </thead>
                    <xsl:for-each select="//PRICELIST/PRICE">
                        <tr class="price-string" data-id="{@node}-{@field}">
                            <td style="padding: 5px 0px"><xsl:value-of select="@node"/></td>
                            <td><xsl:value-of select="."/></td>
                            <td width="80px" class="extkey"><xsl:value-of select="@extkey"/></td>
                            <td>
                                <xsl:variable name="rlm" select="@realm"/>
                                <a href="/{//SECTION[@realm=$rlm]/@name}"><xsl:value-of select="//SECTION[@realm=$rlm]/@title"/></a>
                            </td>
                            <td class="count"><xsl:value-of select="@count"/></td>
                            <td class="price1"><xsl:value-of select="@price1"/></td>
                            <td></td>
                        </tr>
                        <tr class="price-edit" id="{@node}-{@field}">
                            <td>
                                <xsl:value-of select="@node"/>
                                <input type="hidden" name="node" value="{@node}"/>
                                <input type="hidden" name="field" value="{@field}"/>
                            </td>
                            <td><xsl:value-of select="."/></td>
                            <td>
                                <input type="text" name="extkey" value="{@extkey}"/>
                            </td>
                            <td>
                                <xsl:variable name="rlm" select="@realm"/>
                                <a href="/{//SECTION[@realm=$rlm]/@name}"><xsl:value-of select="//SECTION[@realm=$rlm]/@title"/></a>
                            </td>
                            <td><input type="text" name="count" value="{@count}"/></td>
                            <td><input type="text" name="price1" value="{@price1}"/></td>
                            <td align="center">
                                <img src="/i/check.png" width="16px" class="save-form" data-id="{@node}-{@field}"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
                <xsl:apply-templates select="//PRICELIST/PAGING"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>