<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output encoding="utf-8" cdata-section-elements="html" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
    <xsl:template match="/DOCUMENT/MODULE/PANELXML">
        <xsl:text disable-output-escaping="yes"><![CDATA[
            <script type="text/javascript">
            $(document).ready(function(){

            });
            </script>
        ]]></xsl:text>
        <xsl:choose>
            <xsl:when test="//REALM/@name='panel/module/cart/order'">
                <div>Заказ № <xsl:value-of select="//PANELXML/ORDER/@id"/></div>
                <div>
                    <form name="status" action="" method="get">
                        <input type="hidden" name="id" value="{//PANELXML/ORDER/@id}"/>
                        <select name="status" onchange="this.form.submit();">
                            <xsl:variable name="statusroot" select="//VAR[@name='statuslist']"/>
                            <xsl:for-each select="//LIST[@id=$statusroot]/LIST">
                                <option value="{@id}">
                                    <xsl:if test="//PANELXML/ORDER/FIELD[@name='status'] = @id">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="@value"/>
                                </option>
                            </xsl:for-each>
                        </select>
                    </form>
                </div>
                <!--div>
                    Примечание
                    <form method="post">
                        <input type="text" name="adds" value="{//ORDER/ADDS}" /><input type="hidden" name="id" value="{//PANELXML/ORDER/@id}"/>
                        <input type="submit"/>
                    </form>
                </div-->
                <xsl:choose>
                    <xsl:when test="//VAR[@name='ordertype']='noreg'">
                        <div>Имя клиента: <xsl:value-of select="//PANELXML/ORDER/FIELD[@name='uname']"/></div>
                        <div>Телефон: <xsl:value-of select="//PANELXML/ORDER/FIELD[@name='phone']"/></div>
                    </xsl:when>
                    <xsl:otherwise>
                        <table>
                            <tr>
                                <td>Логин</td>
                                <td><xsl:value-of select="//PANELXML/USER/@login"/></td>
                            </tr>
                            <tr>
                                <td>Дата заказа</td>
                                <td><xsl:value-of select="//PANELXML/ORDER/@date"/></td>
                            </tr>
                        </table>
                    </xsl:otherwise>
                </xsl:choose>
                <table class="table">
                    <thead>
                        <th>id</th>
                        <th>Название</th>
                        <th>Кол-во</th>
                        <th>Цена</th>
                        <th>Итого</th>
                    </thead>
                    <xsl:for-each select="//PANELXML/ORDER/ORDERITEM">
                        <tr>
                            <td><xsl:value-of select="@node"/></td>
                            <td><xsl:value-of select="NODE//FIELD[@name='title']/DATA"/></td>
                            <td><xsl:value-of select="@count"/>&#xA0;шт.</td>
                            <td><xsl:value-of select="NODE//FIELD[@name='price']/FIELD[@name='price1']"/></td>
                            <td><xsl:value-of select="NODE//FIELD[@name='price']/FIELD[@name='price1'] * @count"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
                <div>
                    <a class="btn btn-primary" href="/panel/module/cart">Назад</a>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <table cellpadding="0px" cellspacing="0px" border="0px" width="100%" class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th width="10px">№</th>
                            <th>Дата</th>
                            <th>Логин</th>
                            <th>Сумма заказа</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <xsl:for-each select="//ORDERS/ORDER">
                        <tr>
                            <td style="padding: 5px" valign="top">
                                <xsl:value-of select="FIELD[@name='id']"/>
                            </td>
                            <td style="padding: 5px" valign="top">
                                <xsl:value-of select="FIELD[@name='datetime']"/>
                            </td>
                            <td style="padding: 5px" valign="top">
                                <xsl:choose>
                                    <xsl:when test="//VAR[@name='ordertype'] = 'noreg'">
                                        <xsl:value-of select="FIELD[@name='uname']"/>&#xA0;<xsl:value-of select="FIELD[@name='phone']"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <a href="/panel/edituser?userlogin={FIELD[@name='login']}"><xsl:value-of select="FIELD[@name='login']"/></a>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td style="padding: 5px" valign="top">
                                <xsl:value-of select="FIELD[@name='total']"/>&#xA0;тнг.
                            </td>
                            <td style="padding: 5px" valign="top">
                                <xsl:variable name="status" select="FIELD[@name='status']"/>
                                <xsl:value-of select="//LIST[@id=$status]/@value"/>
                            </td>
                            <td>
                                <a href="/panel/module/cart/order?id={@id}" class="openModal-lg" modal-title="Заказ">
                                    <i class="fa fa-edit"/>
                                </a>&#xA0;
                                <a href="/panel/module/cart?delete=1&amp;id={@id}">
                                    <i class="fa fa-times"/>
                                </a>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>