<?
include_once('core/image.lib.php');
#Admin library. version 1.0
class xpAdmin
{
    var $url;
    var $Cmd;
    
    function xpAdmin($uri)
    {
        $this->uri = $uri;
        $cmd = explode('/',$uri);
        $this->Cmd = $cmd[sizeof($cmd) - 1];

    }    
    function GetXML($curr_realm)
	{
        global $DB, $cfg, $Processor, $Cache, $Session, $Config, $Error;
        $view = new Viewer();
        $xml .= "<INCLUDES>";
        $rs = $DB->Execute("SELECT * FROM T_INCLUDE");
        while(!$rs->EOF)
        {
            $xml .= "<INCLUDE include=\"{$rs->fields['include']}\" module=\"{$rs->fields['module']}\" class=\"{$rs->fields['class']}\" title=\"{$rs->fields['title']}\" xsl=\"{$rs->fields['xsl']}\" dash=\"{$rs->fields['dash']}\"/>";
            $rs->MoveNext();
        }
        $xml .= "</INCLUDES>";
        
        switch($this->Cmd)
        {
            case 'clear_cache':
                $dir = $cfg[cachedir];
                $objects = scandir($dir);
                foreach($objects as $obj)
                {
                    if($obj != "." && $obj != "..")
                    {
                        unlink($dir."/".$obj);
                    }    
                }
                header("Location: /panel");
            break;
            case 'addnode':            
                $xmls = scandir("datatypes");
                $xml = "<DATATYPES>";
                foreach($xmls as $object)if($object != "." && $object != "..")$xml .= str_replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "", file_get_contents('datatypes/'.$object));
                $xml .= "</DATATYPES>"; 
                $xml .= "<LISTS>".$view->LoadListData(1)."</LISTS>";
                if($_POST)
                {
                    //print_r($_POST);
                    $nodedata = Array();
                    $class = $_POST['class'];
                    $name = $_POST['name'];
                    $realm = $_POST['realm'];
                    $login = $Session->login;
                    $disabled = $_POST['disabled'];
                    $topcounter = time();
                    //$title = $_POST['title'];
                    //print_r($view->ValidateNodeData($class, $node));
                    if($disabled != 1)$disabled = 0;
                    if($_POST['highlight'] != '')$highlight = strtotime($_POST['highlight']);
                    else $highlight = '';
                    $DB->Execute("INSERT INTO T_NODE (name, class, login, disabled, highlight, topcounter) VALUES (?,?,?,?,?,?)", Array($name, $class, $login, $disabled, $highlight, $topcounter));
                    $node = $DB->Identity();
                    $mode = 0;
                    $this->SetNodeRealm($node, $realm, $mode);
                    $valid_data = $view->ValidateInputData($class, $node);
                    $this->SaveNodeData($node, $valid_data);
                    $this->DeleteNodeCache($node);
                    $this->DeleteTreeCache();
                    header("Location: /panel?parent=".$realm);          
                }
            break;
            case 'editnode':
                if($_GET['node'])$xml .= $view->LoadNodeData($_GET[node]);
                if($_GET['upnode'])
                {
                    $topcounter = time();
                    $DB->Execute("UPDATE T_NODE SET topcounter=? WHERE node=?", Array($topcounter, $_GET['upnode']));
                    header("Location: /panel?node=".$_GET['upnode']);
                }
                if($_GET['atrealm'])
                {
                    $DB->Execute("DELETE FROM T_NODE_REALM WHERE realm=? AND node=? AND mode=1 LIMIT 1", Array($_GET['atrealm'], $_GET['node']));
                    header("Location: /panel?node=".$_GET['node']);
                    
                }
                if($_POST)
                {
                    if($_POST['atrealm'] && $_POST['mode'] && $_POST['node'])
                    {
                        $rs=$DB->Execute("INSERT INTO T_NODE_REALM (realm, mode, node) VALUES (?, ?, ?)", $_POST);
                        header("Location: /panel?node=".$_POST['node']);
                    }
                    else
                    {
                        $nodedata = Array();
                        $class = $_POST['class'];
                        $name = $_POST['name'];
                        $realm = $_POST['realm'];
                        $old_realm = $_POST['old_realm'];//чтобы при переносе с раздела в раздел, кеш и там и там чистился
                        $node = (int)$_POST['node'];
                        $disabled = $_POST['disabled'];
                        if($_POST['highlight'] != '')$highlight = strtotime($_POST['highlight']);
                        else $highlight = '';
                        if($disabled != 1)$disabled = 0;
                        $valid_data = $view->ValidateInputData($class, $node);
                        $this->DeleteNodeRealm($node, 0);
                        $this->SetNodeRealm($node, $realm, 0);
                        $this->DeleteNodeData($node);
                        $this->SaveNodeData($node, $valid_data);
                        //var_dump($valid_data);
                        $this->DeleteNodeCache($node);
                        //$this->DeleteRealmCache($realm);
                        //$this->DeleteRealmCache($old_realm);

                        $DB->Execute("UPDATE T_NODE SET class=?, name=?, disabled=?, highlight=? WHERE node=?", Array($class, $name, $disabled, $highlight, $node));
                        header("Location: /panel?parent=".$realm);
                    }
                }
            break;
            case 'addrealm':
                if($_POST)
                {              
                    $name = $_POST['name'];
                    $parent = $_POST['parent'];
                    $title = $_POST['title'];
                    $descr = htmlspecialchars($_POST['descr']);
                    if($_POST['realmpagesize'] != '')$realmpagesize = $_POST['realmpagesize'];
                    else $realmpagesize = $cfg['defaultpagesize'];
                    $auth = 0;
                    $weight = $_POST['weight'];
                    $template = $_POST['template'];
                    $metadescr = $_POST['metadescr'];
                    $metakey = $_POST['metakey'];
                    $rs = $DB->Execute("INSERT INTO T_REALM (name, parent, title, auth, weight, realmpagesize, template, descr, metadescr, metakey) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? ,?)", Array($name, $parent, $title, $auth, $weight, $realmpagesize, $template, $descr, $metadescr, $metakey));
                    $_POST[realm] = $DB->Identity();
                    if ($_FILES['image']['tmp_name'])
                    {
                        $key=$_POST[realm];
                        $image = new xpImage($_FILES['image'], 'image');
                        $upload = $image->imagerealmUpload($key, $cfg['filesdir'].'/realm');
						$_POST[image] = $upload[image];
                    }
                    $rs = $DB->Execute("UPDATE T_REALM SET image=? WHERE realm=?", Array($_POST[image], $_POST[realm]));
                    //$this->DeleteRealmCache($parent);
                    $this->DeleteTreeCache();
                    header("Location: /panel?parent=".$parent);
                }
            case 'editrealm':
                $realm = $_GET['realm'];
                $rs = $DB->Execute("SELECT * FROM T_REALM WHERE realm=?", Array($realm));
                $xml .= "<REALM_DESCR><![CDATA[".stripslashes(htmlspecialchars_decode($rs->fields['descr']))."]]></REALM_DESCR>";
                $xml .= "<REALM_METADESCR><![CDATA[".stripslashes(htmlspecialchars_decode($rs->fields['metadescr']))."]]></REALM_METADESCR>";
                $xml .= "<REALM_METAKEY><![CDATA[".stripslashes(htmlspecialchars_decode($rs->fields['metakey']))."]]></REALM_METAKEY>";
                if($_POST)
                {              
                    //print_r($_POST);
                    $name = $_POST['name'];
                    $parent = $_POST['parent'];
                    $title = $_POST['title'];
                    $descr = htmlspecialchars($_POST['descr']);
                    $old_realm = $_POST['old_realm'];//чтобы при переносе с раздела в раздел, кеш и там и там чистился
                    $auth = 0;
                    $weight = $_POST['weight'];
                    $realmpagesize = $_POST['realmpagesize'];
                    $realm = $_POST['realm']; 
                    $template = $_POST['template'];
                    $metadescr = $_POST['metadescr'];
                    $metakey = $_POST['metakey'];
                    if ($_FILES['image']['tmp_name'])
                    {
                        if(is_file($_POST[old_image]))unlink($_POST[old_image]);
                        $key=$realm;
                        $image = new xpImage($_FILES['image'], 'image');
                        $upload = $image->imagerealmUpload($key, $cfg['filesdir'].'/realm');
                        $image = $upload[image];
                    }
                    else $image = $_POST[old_image];
                    $rs = $DB->Execute("UPDATE T_REALM SET name=?, parent=?, title=?, auth=?, weight=?, realmpagesize=?, template=?, image=?, descr=?, metadescr=?, metakey=? WHERE realm=?", Array($name, $parent, $title, $auth, $weight, $realmpagesize, $template, $image, $descr, $metadescr, $metakey, $realm));
                    /*$this->DeleteRealmCache($old_realm);
                    $this->DeleteRealmCache($parent);*/
                    $this->DeleteTreeCache();
                    header("Location: /panel?parent=".$parent);
                }
            break;
            case 'deleterealm':
                if($_POST[realm])
                {
                    foreach($_POST[realm] as $k=>$v)
                    {
                        $DB->Execute("DELETE FROM T_REALM WHERE realm=? LIMIT 1", Array($v));
                        $DB->Execute("DELETE FROM T_REALM_GROUP WHERE realm=?", Array($v));
                        $rs=$DB->Execute("SELECT * FROM T_NODE_REALM WHERE realm=? AND mode=0", Array($v));
                        while(!$rs->EOF)
                        {
                            $this->DeleteNodeCache($rs->fields['node']);
                            $foldername = "files/".$rs->fields['node'];
                            if(file_exists($foldername))
                            {
                                $scan = glob($foldername.'/*');
                                foreach($scan as $index=>$path)unlink($path);
                                rmdir($foldername);
                            }
                            $DB->Execute("DELETE FROM T_NODE WHERE node=? LIMIT 1", Array($rs->fields['node']));
                            $DB->Execute("DELETE FROM T_NODE_DATA WHERE node=?", Array($rs->fields['node']));
                            $rs->MoveNext();
                        }
                        $this->DeleteRealmCache($v);
                        $DB->Execute("DELETE FROM T_NODE_REALM WHERE realm=?", Array($v));
                        $rs->close();
                    }
                    $this->DeleteTreeCache();
                    header("Location: /panel?parent=".$_POST['parent']);
                }
            break;
            case 'deletenode':
                if($_GET[node] && $_GET[field] && $_GET[prefix])
                {
                    $rs = $DB->Execute("SELECT dataid FROM T_NODE_DATANAME WHERE dataname = ? LIMIT 1", Array($_GET[field]));
                    $dataid = $rs->fields['dataid'];
                    $rs = $DB->Execute("SELECT value FROM T_NODE_DATA WHERE node=? AND dataid=?", Array($_GET[node], $dataid));
                    $filename = $rs->fields[value];
                    $prefixes = explode(';', $_GET[prefix]);
                    array_pop($prefixes);
                    foreach($prefixes as $v)
                    {
                        $f = $cfg['filesdir']."/node/".$_GET[node]."/".$v.$filename;
                        unlink($f);
                        $DB->Execute("DELETE FROM T_NODE_DATA WHERE node=? AND dataid=?", Array($_GET[node], $dataid));
                        $this->DeleteNodeCache($_GET[node]);
                        header("Location: /panel/editnode?node=".$_GET[node]);
                    }

                }
                if($_POST[node])
                {
                    $parent = $_POST['parent']; 
                    foreach($_POST[node] as $k=>$v)
                    {
                        $rs=$DB->Execute("SELECT * FROM T_NODE_REALM WHERE node=? AND realm=?", Array($v, $parent));
                        $mode = $rs->fields['mode'];
                        $rs->close();
                        if($mode == 0)
                        {
                            $foldername = "files/".$v;
                            if(file_exists($foldername))
                            {
                                $scan = glob($foldername.'/*');
                                foreach($scan as $index=>$path)unlink($path);
                                rmdir($foldername);
                            }
                            $this->DeleteNodeCache($v);
                            $this->DeleteRealmCache($parent);
                            $DB->Execute("DELETE FROM T_NODE WHERE node=?", Array($v));
                            $DB->Execute("DELETE FROM T_NODE_DATA WHERE node=?", Array($v));
                            $DB->Execute("DELETE FROM T_NODE_REALM WHERE node=?", Array($v));
                            $DB->Execute("DELETE FROM T_SEARCH WHERE node=?", Array($v));
                            $DB->Execute("DELETE FROM T_COMENT WHERE node=?", Array($v));
                        }
                        else
                        {
                            $DB->Execute("DELETE FROM T_NODE_REALM WHERE node=? AND realm=? AND mode=1", Array($v, $parent));
                        }
                    }
                    header("Location: /panel?parent=".$parent);
                }
            break;
            case 'users':
                $rs = $DB->Execute("SELECT * FROM T_USERS ORDER BY user ASC");
                $xml .= "<USERS>";
                while(!$rs->EOF)
                {   
                    $xml .= "<USER";
                    foreach($rs->fields as $k => $v)if(!is_integer($k))$xml.=" $k =\"$v\"";
                    $xml.="/>";
                    $rs->MoveNext();
                }
                $xml .= "</USERS>";        
            break;
            case 'adduser':
                if($_POST)
                {
                    $rs=$DB->Execute("SELECT * FROM T_LOGIN WHERE login=? LIMIT 0, 1", Array($_POST['userlogin']));
                    if(!$rs->EOF)
                    {
                        $Error->is_error("a_1", "login exists!");
                        break;
                    }
                    if($_POST['pass'] != $_POST['pass2'])
                    {
                        $Error->is_error("a_2", "passwords not match!");
                        break;
                    }
                    if(!preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,4}/i", $_POST['email']))
					{
						$Error->is_error("a_3", "email not correct!");
                        break;
					}
					$hash = md5($_POST['pass']);
                    $DB->Execute("INSERT INTO T_LOGIN (login, hash) VALUES (?, ?)", Array($_POST['userlogin'], $hash));
                    $DB->Execute("INSERT INTO T_USERS (login, email, name) VALUES (?, ?, ?)", Array($_POST['userlogin'], $_POST['email'], $_POST['name']));
                    header("Location: /panel/users");
                }
            break;
            case 'edituser':
                if($_POST)
                {
                    if($_POST['pass'] != $_POST['pass2'])
                    {
                        $Error->is_error("a_2", "passwords not match!");
                        break;
                    }
                    if(!preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,4}/i", $_POST['email']))
					{
						$Error->is_error("a_3", "email not correct!");
                        break;
					}
					$hash = md5($_POST['pass']);
					if($_POST['pass'] != '')$DB->Execute("UPDATE T_LOGIN SET hash=? WHERE login=?", Array($hash, $_POST['userlogin']));
                    $DB->Execute("UPDATE T_USERS SET email=?, name=? WHERE login=?", Array($_POST['email'], $_POST['name'], $_POST['userlogin']));
                    header("Location: /panel/users");
                }
                $rs = $DB->Execute("SELECT * FROM T_USERS WHERE login=?", Array($_GET[userlogin]));
                if(!$rs->EOF)
                {
                    $xml .= "<USER";
                    foreach($rs->fields as $k => $v)if(!is_integer($k))$xml.=" $k =\"$v\"";
                    $xml.="/>";
                }
                else $Error->is_error("a_4", "Пользователь не существует!");
            break;
            case 'deleteuser':
                if($_GET[userlogin])
                {
                    $DB->Execute("DELETE FROM T_USERS WHERE login=?", Array($_GET[userlogin]));
                    $DB->Execute("DELETE FROM T_LOGIN WHERE login=?", Array($_GET[userlogin]));
                    $DB->Execute("DELETE FROM T_LOGIN_GROUP WHERE login=?", Array($_GET[userlogin]));
                    header("Location: /panel/users");
                }
            break;
            case 'groups':
                $xml .= $this->GetGroupXML();
            break;
            case 'addgroup':
                if($_POST)
                {
                    $DB->Execute("INSERT INTO T_GROUP (groupname, url) VALUES (?, ?)",Array($_POST[groupname], $_POST[url]));
                    header("Location: /panel/groups");
                }
            break;
            case 'editgroup':
                if($_GET[gid])$xml .= $this->GetGroupXML(Array($_GET[gid]));
                if($_POST)
                {
                    $DB->Execute("UPDATE T_GROUP SET groupname=?, url=? WHERE gid=?",Array($_POST[groupname], $_POST[url], $_POST[gid]));
                    header("Location: /panel/groups");
                }
            break;
            case 'editrealmgroup':
                if($_POST)
                {
                    $DB->Execute("DELETE FROM T_REALM_GROUP WHERE realm=?", Array($_POST[realm]));
                    if($_POST[group])foreach($_POST[group] as $k=>$v)$DB->Execute("INSERT INTO T_REALM_GROUP (realm, gid) VALUES (?, ?)", Array($_POST[realm], $v));
                }
                $groups = Array();
                $rs = $DB->Execute("SELECT gid FROM T_REALM_GROUP WHERE realm=?", Array($_GET['realm']));
                while(!$rs->EOF)
                {
                    array_push($groups, $rs->fields[gid]);
                    $rs->MoveNext();
                }
                $xml .= $this->GetGroupXML($groups);
            break;
            case 'editusergroup':
                if($_POST)
                {
                    $DB->Execute("DELETE FROM T_LOGIN_GROUP WHERE login=?", Array($_POST['userlogin']));
                    if($_POST[group])foreach($_POST[group] as $k=>$v)$DB->Execute("INSERT INTO T_LOGIN_GROUP (login, gid) VALUES (?, ?)", Array($_POST[userlogin], $v));
                    header("Location: /panel/users");
                }
                $groups = Array();
                $rs = $DB->Execute("SELECT gid FROM T_LOGIN_GROUP WHERE login=?", Array($_GET['userlogin']));
                while(!$rs->EOF)
                {
                    array_push($groups, $rs->fields[gid]);
                    $rs->MoveNext();
                }
                $xml .= $this->GetGroupXML($groups);
            break;
            case 'deletegroup':
                $DB->Execute("DELETE FROM T_GROUP WHERE gid=?", Array($_GET[gid]));
                $DB->Execute("DELETE FROM T_LOGIN_GROUP WHERE gid=?", Array($_GET[gid]));
                $DB->Execute("DELETE FROM T_REALM_GROUP WHERE gid=?", Array($_GET[gid]));
                header("Location: /panel/groups");
            break;
            case 'lists':
                if(!$_GET['list'])$list='1';
                else $list = $_GET['list'];
                $xml .= $view->LoadListData($list);
            break;
            case 'addlist':
                if($_POST)
                {
                    $trans = array(' '=>'_', '1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '.'=>'.', '-'=>'-',   
                                    'а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d', 'е'=>'e', 'ё'=>'e', 'ж'=>'zh', 'з'=>'z', 'и'=>'i', 'й'=>'i', 'к'=>'k',
                                    'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o', 'п'=>'p', 'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u', 'ф'=>'f', 'х'=>'h', 'ц'=>'c',
                                    'ч'=>'ch', 'ш'=>'sh', 'щ'=>'sch', 'ы'=>'i', 'э'=>'e', 'ю'=>'yu', 'я'=>'ya');
                    $value_chars = $this->split_utf8($_POST['value'], 1);
                    foreach($value_chars as $v) if($trans[$v])$url.=$trans[$v];
                    $rs = $DB->Execute("SELECT url FROM T_LIST WHERE list=? LIMIT 1", Array($_POST['parent']));
                    $parent_url = $rs->fields['url'];
                    if($parent_url != '')$url = $parent_url."/".$url;
                    $DB->Execute("INSERT INTO T_LIST (parent, value, url) VALUES(?,?,?)", Array($_POST['parent'], $_POST['value'], $url));
                    header("Location: /panel/lists");
                }
                if(!$_GET['list'])$list='1';
                else $list = $_GET['list'];
                $xml .= $view->LoadListData($list);
            break;
            case 'deletelist':
                $list = $_GET['list'];
                $DB->Execute("DELETE FROM T_LIST WHERE list=? OR parent=?", Array($list, $list));
                header("Location: /panel/lists");
                
            break;
            case 'include':
                 
            break;
            case 'editinclude':
                $xml .= "<INCLUDES>";
                if($_GET['include'])
                {    
                    $realms = array();
                    $rs = $DB->Execute("SELECT * FROM T_INCLUDE_REALM WHERE include=\"{$_GET['include']}\"");
                    while(!$rs->EOF)
                    {
                        array_push($realms, $rs->fields['realm']);
                        $rs->MoveNext();   
                    }
                    $rs->close();
                    $realms = array_unique($realms);
                    $xml .= "<REALMS>";
                    $rs=$DB->Execute("SELECT a.*, b.mode FROM T_REALM as a LEFT JOIN T_INCLUDE_REALM as b ON a.realm = b.realm WHERE a.realm IN ("."'".implode("','" , $realms)."'".") AND b.include=\"{$_GET['include']}\"");
                    while(!$rs->EOF)
                    {
                        $xml .= "<REALM";
            			foreach($rs->fields as $k => $v)if(!is_integer($k) && $k != 'descr')$xml.=" $k =\"$v\"";
            			$xml.="/>";
            		    $rs->MoveNext();
                    }
                    $rs->close();
                    $xml .= "</REALMS>";
                }
                $xml .= "</INCLUDES>";
                if($_POST)
                {
                    if($_POST['addrealminc'])
                    {
                        $realm = $_POST['realm'];
                        $mode = $_POST['mode'];
                        $include = $_POST['include'];
                        $query = "INSERT INTO T_INCLUDE_REALM (include, realm, mode) VALUES (\"{$include}\", \"{$realm}\", \"{$mode}\")";
                        $DB->Execute($query);
                        header("Location: /panel/include?include=".$include);
                    }
                    else
                    {
                        $title = $_POST['title'];
                        $include = $_POST['include'];
                        if($_POST['dash'] == 'on')$dash = $_POST['dash'];
                        else $dash = 'off';
                        //var_dump($_POST);
                        $DB->Execute("UPDATE T_INCLUDE SET title=?, dash=? WHERE include=?", Array($title, $dash, $include));
                        header("Location: /panel/include");
                    }    
                }
            break;
            case 'insertinclude':
                if($_POST)
                {
                    $DB->Execute("INSERT INTO T_INCLUDE (module, class, title, xsl, dash) VALUES (\"{$_POST['module']}\", \"{$_POST['class']}\", \"{$_POST['title']}\", \"{$_POST['xsl']}\", \"{$_POST['dash']}\")");
                    //print_r($_POST);
                    header("Location: /panel/include");   
                }
            break;
            case 'deleteinclude':
                if($_POST['include'])
                {   
                    $DB->Execute("DELETE FROM T_INCLUDE WHERE include IN ("."'".implode("','" , $_POST['include'])."'".")");
                    $DB->Execute("DELETE FROM T_INCLUDE_REALM WHERE include IN ("."'".implode("','" , $_POST['include'])."'".")");
                    header("Location: /panel/include");
                }
                if($_POST['realm'])
                {
                    //$DB->Execute("DELETE FROM T_INCLUDE_REALM WHERE include = \"{$_POST['include']}\" AND realm IN ("."'".implode("','" , $_POST['realm'])."'".")");
                    //echo "DELETE FROM T_INCLUDE_REALM WHERE include = \"{$_POST['include']}\" AND realm IN ("."'".implode("','" , $_POST['realm'])."'".")";
                    $arr = array();
                    $include = $_POST['includerealm'];
                    $DB->Execute("DELETE FROM T_INCLUDE_REALM WHERE include=\"{$include}\" AND realm IN ("."'".implode("','" , $_POST['realm'])."'".")");
                    header("Location: /panel/include?include=".$include);
                }
            break;
            case 'module':
                if($_GET['module'])
                {
                    $rs = $DB->Execute("SELECT * FROM T_INCLUDE WHERE include=? OR class=?", Array($_GET['module'],$_GET['module']));
                    $ns = strtolower($rs->fields['class']);
                    if($rs && !$rs->EOF)
                    {
                        $xml .= "<MODULE ";
                        foreach($rs->fields as $k=>$v)$xml .= $k."=\"".$v."\" ";
                        $xml .= ">";
                        eval("\$Object = new {$rs->fields['class']}(\$this->uri, \$curr_realm);");
        				if(isset($Object))
                        {
                            $xml .= $Object->GetPanelXML($realm);
                            if(method_exists($Object, '_gettemplate'))
                            {
                                $modxsl = $Object->_gettemplate();
                                $Processor->Template->addtemplate($modxsl);
                                $modxsl = $Object->_gettemplate('default');
                                $Processor->Template->addtemplate($modxsl);
                            }
                        }
                        $rs->MoveNext();
                        $rs->close();
                        switch($_GET['action'])
                        {
                            case 'addval':
                                $res = $Config->SetVar($ns, $_POST[key], $_POST[value]);
                                header("Location: /panel/module?module=".$_GET['module']);
                                break;
                            case 'editval':
                                $res = $Config->EditVar($ns, $_POST[key], $_POST[value]);
                                header("Location: /panel/module?module=".$_GET['module']);
                                break;
                            case 'delval':
                                $res = $Config->DelVar($ns, $_GET[key]);
                                header("Location: /panel/module?module=".$_GET['module']);
                                break;
                            default:
                                $xml .= "<VARS ns=\"".$ns."\" >";
                                $conf = $Config->Getmoduleconf($ns);
                                if($conf != NULL)foreach($conf as $k=>$v)$xml .= "<VAR name=\"".$k."\">".$v."</VAR>";
                                $xml .= "</VARS>";
                                break;
                        }
                        $xml .= "</MODULE>";
                    }
                }
            break;
            case 'config':
                $cfg = $Config->Getmoduleconf('global');
                switch($_GET['action'])
                {
                    case 'addval':
                        $res = $Config->SetVar('global', $_POST[key], $_POST[value]);
                        header("Location: /panel/config");                           
                    break;
                    case 'editval':
                        $res = $Config->EditVar('global', $_POST[key], $_POST[value]);
                        header("Location: /panel/config");                           
                    break;
                    case 'delval':
                        $res = $Config->DelVar('global', $_GET[key]);
                        header("Location: /panel/config");                           
                    break;
                    default:
                        $xml .= "<VARS ns=\"global\" >";
                        if($cfg != NULL)foreach($cfg as $k=>$v)$xml .= "<VAR name=\"".$k."\">".$v."</VAR>";
                        $xml .= "</VARS>";
                    break;
                }
            break; 
            case 'constructor':
                $xml = "<CONSTRUCTOR>";
                $xml .= str_replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "", file_get_contents('datatypes/sample.xml'));
                $xml .= "</CONSTRUCTOR>";    
            break;
            default:
                if(!$_GET['parent'])$_GET['parent'] = 1000;
                if(!$_GET['page'])$_GET['page'] = 1;
                
                $xmls = scandir("datatypes");
                $xml .= "<DATATYPES>";
                foreach($xmls as $object)if($object != "." && $object != "..")$xml .= str_replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "", file_get_contents('datatypes/'.$object));
                $xml .= "</DATATYPES>";
                
                $realm = $_GET['parent'];
                $realms = $Processor->GetParents($realm);
                $query = "SELECT * FROM T_INCLUDE_REALM AS a LEFT JOIN T_INCLUDE AS b ON a.include = b.include WHERE (a.realm IN ("."'".implode("','" , $realms)."'".") AND a.mode = 1) OR (a.realm=\"{$realm}\" AND a.mode = 0)";
                $rs=$DB->Execute($query);
                $xml .= "<INCLUDES_REALM>";
                while(!$rs->EOF)
                {
                    $xml .= "<INCLUDE realm=\"{$rs->fields['realm']}\" include=\"{$rs->fields['include']}\" module=\"{$rs->fields['module']}\" class=\"{$rs->fields['class']}\" title=\"{$rs->fields['title']}\" xsl=\"{$rs->fields['xsl']}\" dash=\"{$rs->fields['dash']}\"/>";
                    $rs->MoveNext();
                }
                $xml .= "</INCLUDES_REALM>";
                $rs->close();
                //$xml .= $view->LoadRealmData($realm, $_GET['page']);<br />
                $order[0] = 'topcounter';
                $order[1] = 'DESC';
                $params = array('realm'=>$realm, 'page'=>$_GET['page'], 'disabled'=>0, 'order'=>$order);
                $xml .= $view->LoadNodeSet($params);
            break;
        }
		return $xml;
	}

    function GetGroupXML($current = NULL)
    {
        global $DB;
        if($current == NULL)$current = Array();
        $rs = $DB->Execute("SELECT * FROM T_GROUP");
        $xml .= "<GROUPS>";
        while(!$rs->EOF)
        {
            if(in_array($rs->fields[gid], $current))$curr = "1";
            else $curr = "0";
            $xml .= "<GROUPDATA gid=\"".$rs->fields[gid]."\" url=\"".$rs->fields[url]."\" selected=\"".$curr."\"><![CDATA[".$rs->fields['groupname']."]]></GROUPDATA>";
            $rs->MoveNext();
        }
        $xml .= "</GROUPS>";
        $rs->close();
        return $xml;     
    }

/*    function SaveNodeData($node, $data)
    {
        global $DB;
        foreach($data as $k=>$v)$datanames .= "'".$k."', ";//собираем имена всех полей
        $datanames = substr($datanames, 0, -2);
        $query = "SELECT * FROM T_NODE_DATANAME WHERE dataname IN (".$datanames.")";
        $rs=$DB->Execute($query);
        while(!$rs->EOF)
        {
            $dn = $rs->fields['dataname'];
            $datanames_valid["{$dn}"] = $rs->fields["dataid"];
            $rs->MoveNext();
        }
        $rs->close();
        foreach($data as $k=>$v)
        {
            if(!$datanames_valid[$k]) //если имени поля в базе нету, то добавляем его в базу
            {
                $DB->Execute("INSERT INTO T_NODE_DATANAME (dataname) VALUES (\"$k\")");
                $dataid = $DB->Identity();
                $data_valid[$dataid] = $v;
            }
            else $data_valid["{$datanames_valid[$k]}"] = $v;   
        }
        $query = "INSERT INTO T_NODE_DATA (node, dataid, value) VALUES";
        foreach($data_valid as $k=>$v)$query .= "(\"$node\", \"$k\", \"$v\"),";
        $query = substr($query, 0, -1);
        $DB->Execute($query);
        return TRUE;
    }  */
    
    function SaveNodeData($node, $data)
    {
        global $DB, $Error;
        foreach($data as $k=>$v)$datanames .= "'".$k."', ";//собираем имена всех полей
        $datanames = substr($datanames, 0, -2);
        $query = "SELECT * FROM T_NODE_DATANAME WHERE dataname IN (".$datanames.")";
        $rs=$DB->Execute($query);
        while(!$rs->EOF)
        {
            $dn = $rs->fields['dataname'];
            $datanames_valid["{$dn}"] = $rs->fields["dataid"];
            $rs->MoveNext();
        }
        $rs->close();
        foreach($data as $k=>$v)
        {
            if(!$datanames_valid[$k]) //если имени поля в базе нету, то добавляем его в базу
            {
                $DB->Execute("INSERT INTO T_NODE_DATANAME (dataname) VALUES (\"$k\")");
                $dataid = $DB->Identity();
                if($v != NULL)$data_valid[$dataid] = $v;
            }
            else if($v != NULL)$data_valid["{$datanames_valid[$k]}"] = $v;   
        }
        foreach($data_valid as $k=>$v)
        {
            if(is_array($v))foreach($v as $key=>$value)$DB->Execute("INSERT INTO T_NODE_DATA (node, dataid, value, dataindex) VALUES (?, ?, ?, ?)", Array($node, $k, $value, $key)); 
            else $DB->Execute("INSERT INTO T_NODE_DATA (node, dataid, value) VALUES (?, ?, ?)", Array($node, $k, $v));  
        }
        return TRUE;      
    }
      
    function UpdateNodeData($node, $data)
    {
        global $DB;
        $this->DeleteNodeData($node);
        $this->InsertNodeData($node, $data);
        return TRUE;        
    }    
    function DeleteNodeData($node)
    {
        global $DB;
        $DB->Execute("DELETE FROM T_NODE_DATA WHERE node=?", Array($node));
        $this->DeleteNodeCache($node);
        return TRUE;
    }    
    function SetNodeRealm($node, $realm, $mode=0)
    {
        global $DB;
        $DB->Execute("INSERT INTO T_NODE_REALM (node, realm, mode) VALUES (?, ?, ?)", Array($node, $realm, $mode));
        return TRUE;
    }    
    function DeleteNodeRealm($node, $mode)
    {
        global $DB;
        $DB->Execute("DELETE FROM T_NODE_REALM WHERE node=? AND mode=?", Array($node, $mode));
        return TRUE;
    }    
    function DeleteRealmCache($realm)
    {
        $foldername = "cache/realm/".$realm;
        if(file_exists($foldername))
        {
            $scan = glob($foldername.'/*');
            foreach($scan as $index=>$path)unlink($path);
            rmdir($foldername);
        }
        
        return TRUE;
    }
    function DeleteTreeCache()
    {
        global $cfg;
        $cache = $cfg[cachedir]."/tree".$cfg[root].".xml";
        if(file_exists($cache))unlink($cache);
        return TRUE;
    }
    function DeleteNodeCache($node)
    {
        Global $cfg;
        $path = $cfg[cachedir]."/".$node.".xml";
        if(file_exists($path))unlink($path);
        return TRUE;
    }
    /*function ValidateNodeData($datatype, $node)
	{
	    $result = Array();
	    $view = new Viewer();
        $data = $_POST[$datatype];
        $dt = simplexml_load_file('datatypes/'.$datatype.'.xml');
        $xml = $dt->xpath("//FIELD");        
        foreach($xml as $n)
        {
            $key = $n['name'];
            $value = $data["{$n['name']}"];
            switch($n['type'])
            {
                case 'numeric':
                    $result["{$key}"] = intval($value);
                break;
                case 'string':
                    $result["{$key}"] = $value;
                break;
                case 'text':
                    $result["{$key}"] = $value;
                break;
                case 'datetime':
                    if($value != NULL)$date = strtotime($value);
                    else $date = time();
                    $result["{$key}"] = $date;
                break;
                case 'image':
                    if($_FILES["{$datatype}"]["tmp_name"]["{$key}"] != '')
                    { 
                        $image = new xpImage($_FILES[$datatype], $key);
                        $foldername = "files/".$node;
                        if(!file_exists($foldername))mkdir($foldername); 
                        $result["{$key}"] = $image->imageUpload($key, $foldername);
                        break;
                    }
                    else 
                    {
                        $result["{$key}"] = $data["current_".$key];
                        break;
                    } 
                case 'file':
                    if($_FILES["{$datatype}"]["tmp_name"]["{$key}"] != '')
                    {
                        $foldername = "files/".$node;
                        $filename = $_FILES[$datatype]["tmp_name"]["{$key}"];
                        preg_match('/\.([A-Za-z]*)$/',$_FILES[$datatype]['name']["{$key}"],$ext);
                        $newfilename = "files/".$node."/".$key.$ext[0];
                        if(!file_exists($foldername))mkdir($foldername);
                        $f = file_get_contents($filename);
    		            file_put_contents($newfilename ,$f);
    		            $result["{$key}"] = $key.$ext[0];
                        break;
                    }
                    else
                    {
                        $result["{$key}"] = $data["current_".$key];
                        break;    
                    }
                case 'multilist':
                    $result["{$key}"] = serialize($value);
                break;
                case 'list':
                    $result["{$key}"] = $value;                    
                break;   
            }
        }
        return $result;  
    }
        */
    function rrmdir($dir)
    {
        if(is_dir($dir)) 
        {
            $objects = scandir($dir);
            foreach ($objects as $object)if ($object != "." && $object != "..")if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
            reset($objects);
            rmdir($dir);
        }   
    }
    
    function amstore_xmlobj2array($obj, $level=0) 
    {
        $items = array();
        if(!is_object($obj)) return $items;
        $child = (array)$obj;
        if(sizeof($child)>1) {
            foreach($child as $aa=>$bb) {
                if(is_array($bb)) {
                    foreach($bb as $ee=>$ff) {
                        if(!is_object($ff)) {
                            $items[$aa][$ee] = $ff;
                        } else
                        if(get_class($ff)=='SimpleXMLElement') {
                            $items[$aa][$ee] = $this->amstore_xmlobj2array($ff,$level+1);
                        }
                    }
                } else
                if(!is_object($bb)) {
                    $items[$aa] = $bb;
                } else
                if(get_class($bb)=='SimpleXMLElement') {
                    $items[$aa] = $this->amstore_xmlobj2array($bb,$level+1);
                }
                //echo memory_get_usage() . "\n";
            }
        } else
        if(sizeof($child)>0) {
            foreach($child as $aa=>$bb) {
                if(!is_array($bb)&&!is_object($bb)) {
                    $items[$aa] = $bb;
                } else
                if(is_object($bb)) {
                    $items[$aa] = $this->amstore_xmlobj2array($bb,$level+1);
                } else {
                    foreach($bb as $cc=>$dd) {
                        if(!is_object($dd)) {
                            $items[$obj->getName()][$cc] = $dd;
                        } else
                        if(get_class($dd)=='SimpleXMLElement') {
                            $items[$obj->getName()][$cc] = $this->amstore_xmlobj2array($dd,$level+1);
                        }
                    }
                }
                //echo memory_get_usage() . "\n";
            }
        }
        return $items;
    }
    function Getparentsarray($arr, $par=0)
    {
        $out = Array();
        $parent['id'] = $arr['@attributes']['id'];  
        $parent['name'] = $arr['data'];
        $parent['parent'] = $par;
        $out[$arr['@attributes']['id']] = $parent;
        if($arr['parent'])$out = array_merge($out, $this->Getparentsarray($arr['parent'], $arr['@attributes']['id']));
        return $out;    
    }
    
    function split_utf8($str, $length)
    {
        $result = array();
        for($i = 0; $i < mb_strlen($str); $i += $length)
        {
            $char = mb_substr($str, $i, $length);
            $char =  mb_convert_case($char, MB_CASE_LOWER, "UTF-8"); 
            $result[] = $char; 
        }
        return $result;
    }  
}
?>