<?    
    //include_once('cfg.php');
    include_once('core/error.lib.php');	
    include_once('core/processor.lib.php');  
    include_once('core/db.lib.php');   
    function __autoload($cn)
    {
        global $cfg;
        require_once($cfg[moddir]."/".strtolower($cn).".inc.php");
    }
    
    if(!file_exists('dbcfg.php'))
    {
        echo "Не могу загрузить конфигурационный файл!";
    }
    else
    {    
        include_once('dbcfg.php');
        $Error = new xpError();
        $DB = new xpDatabase();
        $Processor = new xpProcessor();
        $Processor->Run();
    }
?>
