<?
	class Callback extends Module
	{   
        function __construct($data, $realm)
        {
            $this->ns = 'callback';
            parent::__construct($data, $realm);
        } 

		function GetXML()
		{
		    global $cfg;
		    if($_POST)
		    {    
              if($_POST[zakaz]){
                    
                    $subject = '=?UTF-8?B?'.base64_encode("Заказ звонка от посетителя сайта "). "?=".$cfg[name]."";
					$from = '=?UTF-8?B?'.base64_encode("Система уведомлений сайта ".$cfg[name]."" ). "?= <robot@".$cfg[name].">\r\n";
                    $headers.= "From: ".$from."";
					$headers.= "Content-Type: text/html; charset=utf-8;". "\r\n";
					$headers  .= 'MIME-Version: 1.0' . "\r\n";
                    $msg .= "Имя заказавшего звонок: ".$_POST[name]."<br/>";
			        $msg .= "Телефон: ".$_POST[phone]."<br/>";
                    $msg .= "Данное сообщение было отправлено системой уведомлений сайта ".$cfg['name']." отвечать на него не нужно! ";
					$letter ="<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body leftmargin=\"10\" topmargin=\"10\">".$msg."</body></html>";
					mail($cfg[email], $subject, $letter, $headers);
					$xml .= "<CALLBACK message='Сообщение отправлено!'/>";                   
              }
              else
              {              
                    $xml .="<MSG>";
        			if($_POST[mail] && $_POST[name]) 
        			{
                  
    					if(!preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,4}/i", $_POST[mail]))
    					{
    						$xml .= "<CALLBACK message='введён некорректный e-mail!'/>";
    						$xml .="</MSG>";
    						return $xml;
    					}
    					$subject = '=?UTF-8?B?'.base64_encode("Сообщение от посетителя сайта "). "?=".$cfg[name].""; 
    					$from = '=?UTF-8?B?'.base64_encode("".$_POST[name]."")."?= <".$_POST[mail].">\r\n";
                        $headers.= "From: ".$from."";
    					$headers.= "Content-Type: text/html; charset=utf-8;". "\r\n";
					    $headers .= 'MIME-Version: 1.0' . "\r\n";
    					$letter ="<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body leftmargin=\"10\" topmargin=\"10\">".$_POST[name]." пишет:<br/>".$_POST[msg]."<br/><br/>Контактный телефон: ".$_POST[phone]."<br/>Контактный e-mail: ".$_POST[mail]."</body></html>";
    					mail($cfg[email], $subject, $letter, $headers);
    					$xml .= "<CALLBACK message='Сообщение отправлено!'/>";
                    }    			
        			else $xml .= "<CALLBACK message='Не все поля заполнены!'/>";
                    $xml .="</MSG>";
              }
    		}
			return $xml;
		}
	}
?>