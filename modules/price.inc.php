<?
class Price extends Module
{
    var $pagesize;
    var $dt;
    function __construct($data, $realm)
    {
        $this->ns = 'price';
        parent::__construct($data, $realm);
        $this->pagesize = $this->Config['pagesize'];
        $this->dt = $this->Config['dt'];
    }

    function GetPanelXML()
    {
        global $DB, $cfg, $Processor, $Cache, $Session, $Error;
        $view = new Viewer();
        $this->cmd = explode('/',$_GET['action']);
        $cmd = array_shift($this->cmd);
        $xml .= "<PANELXML ns=\"".$this->ns."\" action=\"{$_GET['action']}\" leftmenu=\"hide\">";
        switch($cmd)
        {
            case 'loadprice':
                if($_FILES['price'])
                {
                    $xmlprice = simplexml_load_file($_FILES['price']['tmp_name']);
                    $itemsarray = Array();
                    $realmsarray = Array();
                    foreach($xmlprice as $k=>$v)
                    {
                        $arr = $this->amstore_xmlobj2array($v);
                        $id = $arr['@attributes']['id'];
                        foreach($arr['field'] as $k=>$v)
                        {
                            if($v['data']!='')
                            {
                                $itemsarray[$id][$v['@attributes']['name']] = $v['data'];
                            }
                        }
                    }
                    unset($xmlprice);

                    foreach($itemsarray as $k=>$v)
                    {
                        //////Array ( [Наименование] => '' [Цена] => '' [Кол] => '' [Описание] => '' [Редакция] => '' [parent] => '')
                        $rss = $DB->Execute("SELECT * FROM T_PRICE WHERE extkey = ?", Array($k));
                        if($rss->fields)
                        {
                            $node = $rss->fields['node'];
                            //ЗАПИСЬ ОБНОВЛЯЕМ
                            if((int)$v['Цена'] < 1)$price = 99999999;
                            else $price = (int)$v['Цена'];
                            $DB->Execute("UPDATE T_PRICE SET count=?, price1=?, price2=? WHERE node = ?", Array($v['количество'], $price, $v['Цена1'], $node));

                            //$DB->Execute("UPDATE T_NODE_DATA SET value=? WHERE node=? AND dataid=?", Array($v['Цена'], $node, $costid));
                            //$DB->Execute("UPDATE T_NODE_DATA SET value=? WHERE node=? AND dataid=?", Array($v['Редакция'], $node, $redid));
                        }
                        $rss->close();
                        /*$cache = "cache/node/".$node.".xml";
                        if(is_file($cache))unlink($cache);*/
                    }
                    $xml .= "<RESULT>Прайс Обновлен</RESULT>";
                    header("Location: /panel/price");
                }
                break;
            default :
                $_GET[page] ? $page=$_GET[page] : $page=1;
                $pricenodes = Array();
                $DB->Execute("INSERT INTO T_PRICE (node) SELECT a.node FROM T_NODE AS a WHERE a.class IN (".$this->dt.") AND NOT EXISTS(SELECT * FROM T_PRICE as aa WHERE aa.node=a.node)");
                $DB->Execute("DELETE FROM T_PRICE WHERE node NOT IN(SELECT node FROM T_NODE WHERE class IN (".$this->dt."))");
                $rs = $DB->Execute("SELECT count(*) FROM T_PRICE");
                $countprice = $rs->fields['count(*)'];
                if($countprice>$this->pagesize)
                {
                    $pages = ceil($countprice/$this->pagesize);
                    $next = $page + 1;
                    $prev = $page - 1;

                    $paging .= "<PAGING pages=\"$pages\" page=\"$page\">";
                    if($page != 1)$paging .= "<FIRST page=\"1\"/><PREV page=\"$prev\"/>";
                    for($i = 1; $i <= $pages; $i++)$paging .= "<PAGE number=\"$i\"/>";
                    if($page != $pages)$paging .= "<NEXT page=\"$next\"/><LAST page=\"$pages\"/>";
                    $paging .= "</PAGING>";
                    $from = ($page-1)*$this->pagesize;
                    $to = $page*$this->pagesize;
                    $query = "SELECT * FROM T_PRICE ORDER BY node DESC LIMIT ".$from.", ".$to;
                }
                else $query = "SELECT * FROM T_PRICE ORDER BY node DESC";
                $rs = $DB->Execute($query);
                $xml .= "<PRICELIST>";
                while(!$rs->EOF)
                {
                    $xml .= "<PRICE";
                    foreach($rs->fields as $k => $v)if(!is_integer($k))$xml.=" $k =\"$v\"";
                    $rss=$DB->Execute("SELECT a.value, b.realm FROM T_NODE_DATA as a LEFT JOIN T_NODE_REALM AS b ON a.node=b.node WHERE a.node=? AND a.dataid=(SELECT dataid FROM T_NODE_DATANAME WHERE dataname='title')", Array($rs->fields['node']));
                    $xml .= " realm=\"".$rss->fields[realm]."\">";
                    $xml .= "<![CDATA[".$rss->fields[value]."]]>";
                    //$xml .= $view->LoadNodeData($rs->fields['node']);
                    $xml .= "</PRICE>";
                    $rs->MoveNext();
                }
                $rs->close();
                $xml .= $paging;
                $xml .= "</PRICELIST>";
                $xml .= $view->LoadListData('53');
                break;
        }
        $xml .= "</PANELXML>";
        return $xml;
    }

    function GetXML()
    {
        Global $DB, $Session;
        return $xml;
    }

    function GetAjax($data)
    {
        Global $DB, $Error, $cfg, $Session;
        if(is_array($data))$DB->Execute("UPDATE T_PRICE SET extkey=?, count=?, price1=? WHERE node=?", Array($data['extkey'],$data['count'],$data['price1'],$data['node']));
        return 'ok';
    }
}
?>