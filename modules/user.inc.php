<?
	class User extends Module
	{   
        function __construct($data, $realm)
        {
            $this->ns = 'user';
            parent::__construct($data, $realm);
        } 
		function GetXML()
		{
			global $Session, $Processor, $DB, $Error, $cfg;
			switch($this->Cmd)
			{
                case 'cabinet':
                    if($_POST)
                    {
                        if($_POST['pass'] != $_POST['pass2'])
                        {
                            $Error->is_error("a_2", "passwords not match!");
                            break;
                        }
                        if(!preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,4}/i", $_POST['email']))
    					{
    						$Error->is_error("a_3", "email not correct!");
                            break;
    					}
    					$hash = md5($_POST['pass']);
    					if($_POST['pass'] != '')$DB->Execute("UPDATE T_LOGIN SET hash=? WHERE login=?", Array($hash, $_POST['userlogin']));
    					/*if($_FILES['avatar']['tmp_name'])
                        {
                            $key=$Session->login;
                            $image = new xpImage($_FILES['avatar'], 'avatar');
                            $upload = $image->_imageUpload($key, 'av_', 'files/users', '120', '120');
    						$_POST[avatar] = "/".$upload;
                        }
                        else $_POST[avatar]=$_POST[avaold]; */
                        //$DB->Execute("UPDATE T_USERS SET email=?, name=?, phone=?, avatar=?, address=?, company=? WHERE login=?", Array($_POST['email'], $_POST['name'], $_POST['phone'], $_POST['avatar'], $_POST['address'], $_POST['company'], $_POST['userlogin']));
                        $login = $_POST['userlogin'];
                        $email = mysql_real_escape_string($_POST['email']);
                        $name = mysql_real_escape_string($_POST['name']);
                        $bin = mysql_real_escape_string($_POST['bin']);
                        $address = mysql_real_escape_string($_POST['address']);
                        $company = mysql_real_escape_string($_POST['company']);
                        $phone = mysql_real_escape_string($_POST['phone']);
                        $postindex = mysql_real_escape_string($_POST['postindex']);
                        $country = mysql_real_escape_string($_POST['country']);
                        $obl = mysql_real_escape_string($_POST['obl']);
                        $city = mysql_real_escape_string($_POST['city']);
                        $street = mysql_real_escape_string($_POST['street']);
                        $build = mysql_real_escape_string($_POST['build']);
                        $app = mysql_real_escape_string($_POST['app']); 
                        $DB->Execute("UPDATE T_USERS SET email=?, name=?, address=?, company=?, phone=?, postindex=?, country=?, obl=?, city=?, street=?, build=?, app=?, bin=? WHERE login=?", Array($email, $name, $address, $company, $phone, $postindex, $country, $obl, $city, $street, $build, $app, $bin, $login));
                        //header("Location: /cabinet");
                    }
                    $rs=$DB->Execute("SELECT * FROM T_USERS WHERE login=?", Array($Session->login));
                    while(!$rs->EOF)
                    {
                        $xml .= "<USER>";
                        foreach($rs->fields as $k=>$v)
                        {
                            if($k == 'params' && $v != '')
                            {
                                $arr = unserialize($v);
                                foreach($arr as $kk=>$vv)$sub.="<SUBFIELD name=\"{$kk}\">{$vv}</SUBFIELD>";
                                $xml.="<FIELD name=\"{$k}\">{$sub}</FIELD>";
                            }
                            else $xml.="<FIELD name=\"{$k}\">{$v}</FIELD>";
                        }
                        $xml .= "</USER>";
                        $rs->MoveNext();
                    }
                    $rs->close();
                break;
                case 'reg':
                    if($_POST['registration'])
                    {
                        if($_POST['userlogin'] && $_POST['pass'] && $_POST['pass2'] && $_POST['name'] && $_POST['phone'] && $_POST['email'])
                        {
                            if( md5($cfg['sault'].strtoupper($_POST[cc])) != $_POST[cchash])
                            {
                            	$xml .= "<MSG>Введён некорректный код проверки</MSG>";
                                break;
                            }
                            if(!preg_match("/[0-9a-zA-Z_-]{3,16}/", $_POST['userlogin']))
        					{
        						$xml .= "<MSG>Логин некорректен!(только латинские буквы, цифры, тире, от 3 до 16 знаков)</MSG>";
                                //$Error->is_error("a_3", "email not correct!");
                                //           /^[a-z0-9_-]{3,16}$/
                                break;
        					}
                            $rs=$DB->Execute("SELECT * FROM T_LOGIN WHERE login=? LIMIT 0, 1", Array($_POST['userlogin']));
                            if(!$rs->EOF)
                            {
                                //$Error->is_error("a_1", "login exists!");
                                $xml .= "<MSG>Введенный вами логин уже зарегистрирован!</MSG>";
                                break;
                            }
                            if($_POST['pass'] != $_POST['pass2'])
                            {
                                $xml .= "<MSG>Введенные вами пароли не совпадают!</MSG>";
                                //$Error->is_error("a_2", "passwords not match!");
                                break;
                            }
                            if(!preg_match("/[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,4}/i", $_POST['email']))
        					{
        						$xml .= "<MSG>Email некорректен!</MSG>";
                                //$Error->is_error("a_3", "email not correct!");
                                break;
        					}
        					$hash = md5($_POST['pass']);
                            $DB->Execute("INSERT INTO T_LOGIN (login, hash) VALUES (?, ?)", Array($_POST['userlogin'], $hash));
                            $DB->Execute("INSERT INTO T_USERS (login, email, name, phone, company, postindex, country, obl, city, street, build, app, bin, regdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Array($_POST['userlogin'], $_POST['email'], $_POST['name'], $_POST['phone'], $_POST['company'], $_POST['postindex'], $_POST['country'], $_POST['obl'], $_POST['city'], $_POST['street'], $_POST['build'], $_POST['app'], $_POST['bin'], time()));
                            $DB->Execute("INSERT INTO T_LOGIN_GROUP (login, gid) VALUES (?, 0)", Array($_POST['userlogin']));
                            $DB->Execute("INSERT INTO T_LOGIN_GROUP (login, gid) VALUES (?, 1)", Array($_POST['userlogin']));
                            
                            $subject = "Регистрация на сайте ".$cfg[name];
        					$headers = "From:\"".$cfg[email]."\r\n";
        					$headers.= "Content-Type: text/html; charset=utf-8; ";
        					$headers .= "MIME-Version: 1.0 ";
                            $msg = "Привествуем Вас, ".$_POST['name']." (".$_POST['userlogin'].")! Вы зарегистрировались на сайте ".$cfg['url']." Теперь вы можете войти в Личный кабинет, введя логин и пароль в правой части сайта в блоке \"Личный кабинет\"";
        					$letter ="<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body leftmargin=\"10\" topmargin=\"10\">".$msg."</body></html>";
        					mail($_POST['email'], $subject, $letter, $headers);
                            $xml .= "<MSG>Вы успешно зарегистрировались на сайте, зайдите в Личный кабинет (справа снизу), чтобы получить расширенные возможности</MSG>";
                            $Session->Auth($_POST['userlogin'], $_POST['pass'], '/cabinet');
                            //header("Location: /");   
                        }
                        else $xml .= "<MSG>Не все поля заполнены!</MSG>"; 
                    }
                break;
            }
			return $xml;
		}
	}
?>
